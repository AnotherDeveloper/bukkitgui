/**
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, you can
 * obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Source and compiled files may only be redistributed if they comply with the
 * mozilla MPL2 license, and may not be monetized in any way, including but not
 * limited to selling the software or distributing it through ad-sponsored
 * channels.
 *
 * @author Jasper C. (Another Developer) <mail@another-developer.org>
 */

package org.another_developer.bukkitgui;

import com.jcraft.jsch.*;
import org.another_developer.bukkitgui.tabs.general.TabGeneral;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;

import static org.another_developer.bukkitgui.BukkitGUI.getStackTrace;
import static org.another_developer.bukkitgui.BukkitGUI.log;

public class Server {

    private static final JSch jsch = new JSch();
    private static Session shellSession;
    private static Session serverSession;
    private static Channel shellChannel;
    private static Channel serverChannel;

    private static String username;
    private static char[] password;

    private static String salt;
    private static String host;
    private static int port;

    private static String java;
    public static String location;
    private static String switches;
    private static String arguments;

    private static String runningArgs;

    private static Process process;

    public static boolean remote = false;
    private static int remoteType;
    private static int serverType;
    private static boolean serverRunning = false;

    private static InputStream shellInputStream;
    private static PipedOutputStream shellInput;
    private static OutputStream shellOutputStream;
    private static PipedInputStream shellOutput;

    private static BufferedReader bufferedShellReader;

    private static OutputStream serverInputStream;
    private static InputStream serverRInputStream;
    private static PipedInputStream serverInput;
    private static PipedOutputStream serverRInput;
    private static InputStream serverOutputStream;
    private static OutputStream serverROutputStream;
    private static PipedOutputStream serverOutput;
    private static PipedInputStream serverROutput;

    private static BufferedReader bufferedServerReader;

    /**
     * Starts server and uses the cached settings to start the server.
     * 
     * @param location Should be the location of server jar
     */
    public static void startServer(String location) {
        if (java.isEmpty()) {
            java = System.getProperty("java.home") + File.separator + "bin" + File.separator + "java";
        }
        
        startServer(remote, remoteType, serverType, java, arguments, location, switches);
    }

    /**
     * Starts server and updates all settings that important to the server and if
     * the server is remote or local.
     * 
     * @param remote Is the server remote or not
     * @param remoteType Remote connection type (SSH = 0, RemoteToolKit = 1, JSONAPI = 2, and RemoteBukkit = 3)
     * @param serverType Server type (Vanilla = 0, Bukkit = 1, Spigot = 2, Sponge = 3, Glowstone = 4)
     * @param java Location of the java executable (JRE or JDK)
     * @param location Location of the server jar
     * @param arguments JVM arguments
     * @param switches Server switches
     */
    public static void startServer(boolean remote, int remoteType, int serverType, String java, String arguments, String location, String switches) {
        Server.remote = remote;
        Server.remoteType = remoteType;
        Server.serverType = serverType;
        Server.java = java;
        Server.arguments = arguments;
        Server.location = location;
        Server.switches = switches;
        
        if (remote) {
            connectToServer(username, password, salt, host, port);
        }
        
        if (!switches.contains("nogui")) {
            Server.switches = "nogui " + switches;
        }

        if (!serverRunning) {
            serverLoop();
        }
        
        if (!remote) {
            try {
                shellInput.close();
                shellInputStream.close();
                shellOutput.close();
                shellOutputStream.close();

                serverRInput.close();
                serverRInputStream.close();
                serverROutput.close();
                serverROutputStream.close();

                bufferedShellReader.close();
                bufferedServerReader.close();

                shellChannel.disconnect();
                serverChannel.disconnect();

                shellSession.disconnect();
                serverSession.disconnect();

                remote = false;

                log.log(Level.INFO, "Disconnected from server");
            } catch (NullPointerException ex) {
            } catch (IOException ex) {
            } catch (Exception ex) {
                log.log(Level.SEVERE, getStackTrace(ex));
            }
        }
    }

    /**
     * Updates all settings that important to the server and if the server is remote 
     * or local.
     * 
     * @param remote Is the server remote or not
     * @param java Location of the java executable (JRE or JDK)
     * @param location Location of the server jar
     * @param arguments JVM arguments
     * @param switches Server switches
     */
    public static void updateArgs(boolean remote, String java, String arguments, String location, String switches) {
        Server.remote = remote;
        Server.java = java;
        Server.arguments = arguments;
        Server.location = location;
        Server.switches = switches;
        
        if (!switches.contains("nogui")) {
            Server.switches = "nogui " + switches;
        }
    }
    
    /**
     * Disconnects if there is still a remote instance.
     */
    public static void disconnectRemote() {
        if (remote) {
            try {
                shellInput.close();
                shellInputStream.close();
                shellOutput.close();
                shellOutputStream.close();

                serverRInput.close();
                serverRInputStream.close();
                serverROutput.close();
                serverROutputStream.close();

                bufferedShellReader.close();
                bufferedServerReader.close();

                shellChannel.disconnect();
                serverChannel.disconnect();

                shellSession.disconnect();
                serverSession.disconnect();

                remote = false;

                log.log(Level.INFO, "Disconnected from server");
            } catch (NullPointerException ex) {
            } catch (IOException ex) {
            } catch (Exception ex) {
                log.log(Level.SEVERE, getStackTrace(ex));
            }
        }
    }

    /**
     * Sends command "/stop" to server.
     */
    public static void stopServer() {
        if (serverRunning) {
            sendCommandToServer("/stop");
            serverRunning = false;
        }
    }

    /**
     * Forcefully annihilates the server process.
     */
    public static void killServer() {
        if (!remote && serverRunning) {
            process.destroyForcibly();

            serverRunning = false;
        } else if (serverRunning) {
            //We can open another channel to get the PID of the server and kill it
            //We would be using "ps -ef | grep java [args] -jar ./location nogui switches"

            serverRunning = false;
        }
    }

    /**
     * Sends command "/stop" and then calls method startServer() to restart the server.
     */
    public static void restartServer() {
        stopServer();
        startServer(location);
    }

    /**
     * Sends command "/relaod" to the server
     */
    public static void reloadServer() {
        sendCommandToServer("/reload");
    }

    /**
     * Opens a GUI for remote file system browsing. It will return a string which
     * is the absolute path of the selected jar on the remote file system.
     * 
     * @return 
     */
    //TODO: Make GUI for browsing remote File System
    public static String getServer() {
        try {
            boolean remoteBrowsing = true;
            String remoteFile = "";

            JDialog remoteFileChooser = new JDialog();

            Insets insets = remoteFileChooser.getInsets();

            remoteFileChooser.setTitle("Remote File Chooser");
            remoteFileChooser.setSize(new Dimension(insets.left + insets.right + 510, insets.top + insets.bottom + 350));
            remoteFileChooser.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            remoteFileChooser.setLocationRelativeTo(BaseFrame.baseTabs);
            remoteFileChooser.setVisible(true);

            while (remoteBrowsing && remote) {
                shellInput.write("mkdir /home/jasper/IStillWork\n".getBytes());
                shellInput.flush();
                break;
            }

            remoteBrowsing = false;

            return remoteFile;
        } catch (Exception ex) {
            log.log(Level.SEVERE, getStackTrace(ex));
        }

        return "";
    }

    /**
     * This returns all available java versions on the remote server. You need to be
     * connected first though. Also checks if Screen is available on the remote server.
     * 
     * Screen will allow the server to run detached. In case if the GUI is closed or
     * connection is lost.
     * 
     * @return 
     */
    public static String[] getJava() {
        List<String> itemList = new ArrayList<String>();
        
        while (!remote) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException ex) {
                log.log(Level.SEVERE, getStackTrace(ex));
            }
        }

        try {
            bufferedShellReader = new BufferedReader(new InputStreamReader(shellOutput));
            String line, OS = "";
            boolean parsing = false;

            shellInput.write("\n".getBytes());
            shellInput.flush();

            while ((line = bufferedShellReader.readLine()) != null) {
                if (line.replaceAll("\\[", "").startsWith(Server.username + "@") && OS.equals("")) {
                    shellInput.write("if [ `uname -s` == \"Linux\" ]; then echo \"Linux\"; elif [ `uname -s` == \"FreeBSD\" ]; then echo \"Linux\"; elif [ `uname -s` == \"SunOS\" ]; then echo \"Linux\"; elif [ `uname -s` == \"Darwin\" ]; then echo \"Mac OS X\"; fi;\r".getBytes());
                    shellInput.flush();
                }

                if (line.equals("Linux") || OS.equals("Linux")) {
                    OS = "Linux";
                    String linuxAlt = "";

                    if (line.replaceAll("\\[", "").startsWith(Server.username + "@") && linuxAlt.equals("")) {
                        shellInput.write("if [ -f \"/var/lib/dpkg/alternatives/java\" ]; then echo \"/var/lib/dpkg/alternatives/java\"; else echo \"/var/lib/alternatives/java\"; fi;\r".getBytes());
                        shellInput.flush();
                    }

                    if ((line.equals("/var/lib/dpkg/alternatives/java") || linuxAlt.equals("/var/lib/dpkg/alternatives/java")) && !parsing) {
                        linuxAlt = "/var/lib/dpkg/alternatives/java";
                        parsing = true;

                        shellInput.write("cat /var/lib/dpkg/alternatives/java\r".getBytes());
                        shellInput.flush();
                    } else if ((line.equals("/var/lib/alternatives/java") || linuxAlt.equals("/var/lib/alternatives/java")) && !parsing) {
                        linuxAlt = "/var/lib/alternatives/java";
                        parsing = true;

                        shellInput.write("cat /var/lib/alternatives/java\r".getBytes());
                        shellInput.flush();
                    }

                    if (parsing) {
                        if (line.startsWith("/") && !line.startsWith("/var/lib/") && !line.endsWith(".gz")) {
                            itemList.add(line);
                        }

                        shellInput.write("echo \"Done\"\r".getBytes());
                        shellInput.flush();

                        if (line.equals("Done")) {
                            break;
                        }
                    }
                } else if (line.equals("Mac OS X") || OS.equals("Mac OS X")) {
                    OS = "Mac OS X";
//                    int stage = 0;
//
//                    if (line.contains(username + "$") && stage == 0 && !parsing) {
//                        parsing = true;
//                        
//                        shellInput.write("if [ -d \"/System/Library/Java/JavaVirtualMachines\" ]; then ls /System/Library/Java/JavaVirtualMachines; else echo \"None\"; fi;\r".getBytes());
//                        shellInput.flush();
//                    }
//                    
//                    if (line.contains(username + "$") && stage == 1 && !parsing) {
//                        parsing = true;
//                        
//                        shellInput.write("if [ -d \"/Library/Java/JavaVirtualMachines\" ]; then ls /Library/Java/JavaVirtualMachines; else echo \"None\"; fi;\r".getBytes());
//                        shellInput.flush();
//                    }
//                    
//                    if (line.contains(username + "$") && stage == 2 && !parsing) {
//                        parsing = true;
//                        
//                        shellInput.write(("if [ -d \"/Users/" + username + "/Library/Java/JavaVirtualMachines\" ]; then ls /Users/" + username + "/Library/Java/JavaVirtualMachines; else echo \"None\"; fi;\r").getBytes());
//                        shellInput.flush();
//                    }
//                    
//                    if(parsing) {
//                        
//                    }
                    
                    //TODO: Get JREs and JDKs for Mac OS X
                    //%JAVA_HOME%/Contents/Home/bin/java
                    
                    itemList.add("/usr/bin/java");
                    javax.swing.JOptionPane.showMessageDialog(null, "Do to developers lack of experience with parsing or\n"
                                                                  + "else to say regular expressions, and the lack of not\n"
                                                                  + "having a Mac, remote JVM detection will be disabled\n"
                                                                  + "until a further version of BukkitGUI.", "Warning", JOptionPane.WARNING_MESSAGE);
                    
                    break;
                }
            }
            
            while ((line = bufferedShellReader.readLine()) != null) {
                System.err.println(line);
                
                if (line.replaceAll("\\[", "").startsWith(Server.username + "@") || line.contains(Server.username + "$")) {
                    shellInput.write("if [[ `screen --version` == *\"Screen\"* ]]; then echo \"System has screen\"; else echo \"System does not have screen\"; fi;\r".getBytes());
                    shellInput.flush();
                }
                
                if (line.equals("System has screen")) {
                    //TODO: Check if a screen insance is already running
                    break;
                } else if (line.equals("System does not have screen")) {
                    if (OS.equals("Linux")) {
                        shellInput.write("if [ -f \"/etc/redhat-release\" ]; then cat \"yum\"; elif [ -f \"/etc/arch-release\" ]; then echo \"pacman\"; elif [ -f \"/etc/gentoo-release\" ]; then echo \"emerge\"; elif [ -f \"/etc/SuSE-release\" ]; then echo \"zypp\"; elif [ -f \"/etc/debian_version\" ]; then echo \"apt-get\"; fi;\r".getBytes());
                        shellInput.flush();
                        
                        Object[] options = {"Yes", "No"};
                        int i = JOptionPane.showOptionDialog(null, "The remote system does not have the command \"screen\".\nThis command allows you to close BukkitGUI without\nstopping the server.\n\nWould you like it if I installed it for you?", "Warning", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
                        
                        if (i == 0) {
                            String packagerType = null;
                                
                            if (line.equals("yum") || line.equals("pacman") || line.equals("emerge") || line.equals("zypp") || line.equals("apt-get")) {
                                packagerType = line;
                            }
                            
                            String sudoPassword = null;
                            {
                                JTextField passwordField = (JTextField) new JPasswordField(8);
                                Object[] objects = {passwordField}; 
                                int result = JOptionPane.showConfirmDialog(null, objects, "Enter password for sudo", JOptionPane.OK_CANCEL_OPTION);
                                
                                if (result != JOptionPane.OK_OPTION) {
                                    System.exit(-1);
                                }
                                
                                sudoPassword = passwordField.getText();
                            }
                            
                            shellInput.write(("sudo -S -p '' " + packagerType + " install screen\r").getBytes());
                            shellInput.flush();
                        } else if (i == 1) {
                            break;
                        }
                        
                        break;
                    } else if (OS.equals("Mac OS X")) {
                        JOptionPane.showMessageDialog(null, "We couldn't find the command screen on the remote\nsystem. Maybe this Mac has a version older than\nMaverick 10.9?\n\nWe only use the command so that you can close\nBukkitGUI without stopping the server.", "Command screen not found", JOptionPane.WARNING_MESSAGE);
                        break;
                    }
                }
            }
        } catch (Exception ex) {
            log.log(Level.SEVERE, getStackTrace(ex));
        }

        String[] items = new String[itemList.size()];
        itemList.toArray(items);

        return items;
    }

    /**
     * You send any available MC command to the server.
     * 
     * @param command Minecraft server command, starts with /
     */
    public static void sendCommandToServer(String command) {
        if (serverRunning) {
            try {
                if (command.equals("/stop")) {
                    serverRunning = false;
                }
                
                if (!remote) {
                    serverInputStream.write((command + "\n").getBytes());
                    serverInputStream.flush();
                } else {
                    serverRInput.write((command + "\n").getBytes());
                    serverRInput.flush();
                }
            } catch (IOException ex) {
                log.log(Level.SEVERE, getStackTrace(ex));
            }
        }
    }

    /**
     * This updates the server, but only works when using an SSH connection.
     * 
     * @param server Server version string ex. "1.8.7", "15w14a"
     */
    public static void udpateServer(String server) {
        //TODO: Download and replace current jar, scp jar over if on remote system
        //TODO: Make a list of available version for each server type.

        //Vanilla
        //Weakly releases: http://assets.minecraft.net/
        //Releases (Differs with versions and a and b): https://s3.amazonaws.com/Minecraft.Download/versions/#.#.#/minecraft_server.#.#.#.jar

        //CraftBukkit and Spigot
        //https://hub.spigotmc.org/jenkins/job/BuildTools/
        //https://www.spigotmc.org/threads/spigot-craftbukkit-1-8-7.70074/

        //Sponge (requires a MinecraftForge server)
        //https://repo.spongepowered.org/maven/org/spongepowered/sponge/
        
        //Glowstone

        try {
            String[] url = server.split(": ");
            System.err.println(url[1]);

            if (serverRunning) stopServer();

            if (!remote) {
                String fileName[] = location.split(File.separator);
                new Download(new URL(url[1]), location.replace(fileName[fileName.length - 1], ""), true, "Downloading server");
            } else {

            }
        } catch (Exception ex) {
            
        }
    }

    /* <editor-fold>
     * Starts server and stays in loop for output and input until the server is closed
     *///</editor-fold>
    private static void serverLoop() {
        Runnable serverLoop = new Runnable() {

            @Override
            public void run() {
                try {
                    if (!remote && !serverRunning) {
                        TabGeneral.appendToOutput("Starting local server: " + (java + " " + arguments + " -jar " + location + " " + switches).replace("  ", " "));

                        String fileName[] = location.split(File.separator);

                        ArrayList<String> commandList = new ArrayList<String>();
                        commandList.add(java);
                        commandList.addAll(Arrays.asList(arguments.replace("  ", " ").split(" ")));
                        commandList.add("-jar");
                        commandList.add(location);
                        commandList.addAll(Arrays.asList(switches.replace("  ", " ").split(" ")));
                        
                        ProcessBuilder processBuilder = new ProcessBuilder(commandList);
                        processBuilder.directory(new File(location.replace(fileName[fileName.length - 1], "")));
                        process = processBuilder.start();

                        try {
                            serverInputStream = new PipedOutputStream();
                            serverInput = new PipedInputStream((PipedOutputStream) serverInputStream);

                            serverOutputStream = new PipedInputStream();
                            serverOutput = new PipedOutputStream((PipedInputStream) serverOutputStream);
                        } catch (IOException ex) {
                            log.log(Level.SEVERE, getStackTrace(ex));
                        }

                        serverOutputStream = process.getInputStream();
                        serverInputStream = process.getOutputStream();

                        runningArgs = (java + arguments + " -jar " + location + " " + switches).replace("  ", " ");
                        serverRunning = true;

                        bufferedServerReader = new BufferedReader(new InputStreamReader(serverOutputStream), 1024);
                        String line;

                        System.gc();
                        while ((line = bufferedServerReader.readLine()) != null) {
                            TabGeneral.appendToOutput(line);
                        }

                        bufferedServerReader.close();
                    } else if (!serverRunning && remoteType == 0) {
                        TabGeneral.appendToOutput("Starting remote server: " + (java + " " + arguments + " -jar " + location + " " + switches).replace("  ", " "));

                        String fileName[] = location.split(File.separator);

                        bufferedServerReader = new BufferedReader(new InputStreamReader(serverROutput), 1024);
                        String line;

                        serverRInput.write(("cd \"" + location.replace(fileName[fileName.length - 1], "") + "\"\r").getBytes());
                        serverRInput.write((java + " " + arguments + " -jar \"" + location + "\" " + switches + "\r").replace("  ", " ").getBytes());
                        serverRInput.flush();

                        runningArgs = (java + " " + arguments + " -jar " + location + " " + switches).replace("  ", " ");
                        serverRunning = true;

                        System.gc();
                        while ((line = bufferedServerReader.readLine()) != null) {
                            //FIXME: Remote server output leaks out extra un-needed text
                            if (line.contains("Server") && !line.contains(username + "@") || !line.contains(username + "$") && !line.contains("nogui")) TabGeneral.appendToOutput(line);
                        }

                        bufferedServerReader.close();
                    } else if (!serverRunning && remoteType == 1) {
                        //TODO: Add support for RemoteToolKit
                    } else if (!serverRunning && remoteType == 2) {
                        //TODO: Add support for JSONAPI
                    } else if (!serverRunning && remoteType == 3) {
                        //TODO: Add support for RemoteBukkit
                    }
                } catch (Exception ex) {
                    log.log(Level.SEVERE, getStackTrace(ex));
                }
            }
        };
        Thread t1 = new Thread(serverLoop);
        t1.start();
    }

    /**
     * This will try to connect you to the targeted remote server. It checks your OS for
     * certain SSH files and if they're not there, it creates them. Server gets saved, and
     * certifications as well, as long as the certification are requested.
     * 
     * @param username Remote user username
     * @param password This is the password and has to be a character array
     * @param salt Salt is not used now, but may be used by RemoteToolKit
     * @param host The route to the remote server, either as IP or DNS
     * @param port The port that the remote server is running on or API
     */
    public static void connectToServer(String username, char[] password, String salt, String host, int port) {
        Server.username = username;
        Server.password = password;

        Server.salt = salt;
        Server.host = host;
        Server.port = port;

        try {
            File ssh, known_hosts;

            if (BukkitGUI.OS.contains("Linux") || BukkitGUI.OS.contains("LINUX") || BukkitGUI.OS.contains("linux") || BukkitGUI.OS.contains("Mac OS X")) {
                ssh = new File(System.getProperty("user.home") + "/.ssh");
                known_hosts = new File(System.getProperty("user.home") + "/.ssh/known_hosts");

                if (ssh.exists() && known_hosts.exists()) {
                    jsch.setKnownHosts(System.getProperty("user.home") + "/.ssh/known_hosts");
                } else {
                    if (!ssh.exists()) {
                        ssh.mkdir();
                        known_hosts.createNewFile();
                    } else if (!known_hosts.exists()) {
                        known_hosts.createNewFile();
                    }

                    jsch.setKnownHosts(System.getProperty("user.home") + "/.ssh/known_hosts");
                }
            } else if (BukkitGUI.OS.contains("Windows")) {
                ssh = new File(BukkitGUI.appDir.getAbsolutePath() + "\\.ssh");
                known_hosts = new File(BukkitGUI.appDir.getAbsolutePath() + "\\.ssh\\known_hosts");

                if (ssh.exists() && known_hosts.exists()) {
                    jsch.setKnownHosts(BukkitGUI.appDir.getAbsolutePath() + "\\.ssh\\known_hosts");
                } else {
                    if (!ssh.exists()) {
                        ssh.mkdir();
                        known_hosts.createNewFile();
                    } else if (!known_hosts.exists()) {
                        known_hosts.createNewFile();
                    }

                    jsch.setKnownHosts(BukkitGUI.appDir.getAbsolutePath() + "\\.ssh\\known_hosts");
                }
            }

            //TODO: Give ability to create certs
            String[] privateIdentity = new String[] {"d_dsa", "id_ecdsa", "id_ed25519", "id_rsa"};
            for (String identity : privateIdentity) {
                if (BukkitGUI.OS.contains("Linux") || BukkitGUI.OS.contains("LINUX") || BukkitGUI.OS.contains("linux") || BukkitGUI.OS.contains("Mac OS X")) {
                    if (new File(System.getProperty("user.home") + "/.ssh/" + identity).exists()) {
                        jsch.addIdentity(System.getProperty("user.home") + "/.ssh/" + identity);
                    }
                } else if (BukkitGUI.OS.contains("Windows")) {
                    if (new File(BukkitGUI.appDir.getAbsolutePath() + "\\.ssh\\" + identity).exists()) {
                        jsch.addIdentity(BukkitGUI.appDir.getAbsolutePath() + "\\.ssh\\" + identity);
                    }
                }
            }

            shellSession = jsch.getSession(username, host, port);
            shellSession.setPassword(String.valueOf(password));

            serverSession = jsch.getSession(username, host, port);
            serverSession.setPassword(String.valueOf(password));

            UserInfo ui = new ServerUserInfo() {
                public void showMessage(String message) {
                    JOptionPane.showMessageDialog(null, message);
                }

                public boolean promptYesNo(String message) {
                    Object[] options = {"Yes", "No"};
                    int i = JOptionPane.showOptionDialog(null, message, "Warning", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);

                    return i == 0;
                }
            };

            shellSession.setUserInfo(ui);
            shellSession.connect(30000);

            serverSession.setUserInfo(ui);
            serverSession.connect(30000);

            try {
                shellInputStream = new PipedInputStream();
                shellInput = new PipedOutputStream((PipedInputStream) shellInputStream);

                shellOutputStream = new PipedOutputStream();
                shellOutput = new PipedInputStream((PipedOutputStream) shellOutputStream);

                serverRInputStream = new PipedInputStream();
                serverRInput = new PipedOutputStream((PipedInputStream) serverRInputStream);

                serverROutputStream = new PipedOutputStream();
                serverROutput = new PipedInputStream((PipedOutputStream) serverROutputStream);
            } catch (IOException ex) {
                log.log(Level.SEVERE, getStackTrace(ex));
            }

            shellChannel = shellSession.openChannel("shell");
            shellChannel.setInputStream(shellInputStream);
            shellChannel.setOutputStream(shellOutputStream);
            shellChannel.connect(30000);

            serverChannel= serverSession.openChannel("shell");
            serverChannel.setInputStream(serverRInputStream);
            serverChannel.setOutputStream(serverROutputStream);
            serverChannel.connect(30000);

            Server.remote = true;
        } catch (JSchException ex) {
            if (ex.getMessage().equals("Auth cancel")) {
                JOptionPane.showMessageDialog(null, "The remote server has denied your request", "You were denied", JOptionPane.ERROR_MESSAGE);
            }
            
            log.log(Level.SEVERE, getStackTrace(ex));
        } catch (IOException ex) {
            log.log(Level.SEVERE, getStackTrace(ex));
        }
    }

    /**
     * @deprecated May be added later when I learn more of how to do this on a cross-platform manner.
     * 
     * This creates a GUI and by the parameters given by the user, will try to port-forward.
     */
    public static void setupPortForwarding() {
        //TODO: Study cristopheres code and create own version and GUI
        JOptionPane.showMessageDialog(null, "Port-forwarding isn't available yet,\nit may get added in the future.", "Information", JOptionPane.INFORMATION_MESSAGE);
        log.log(Level.INFO, "User tryed to access port-forwarding");
    }
    
    private static abstract class ServerUserInfo implements UserInfo, UIKeyboardInteractive {

        public String getPassword() {
            return null;
        }

        public boolean promptYesNo(String str) {
            return false;
        }

        public String getPassphrase() {
            return null;
        }

        public boolean promptPassphrase(String message) {
            return false;
        }

        public boolean promptPassword(String message) {
            return false;
        }

        public void showMessage(String message) { }

        public String[] promptKeyboardInteractive(String destination, String name, String instruction, String[] prompt, boolean[] echo) {
            return null;
        }
    }
}