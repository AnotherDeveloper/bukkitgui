/**
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, you can
 * obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Source and compiled files may only be redistributed if they comply with the
 * mozilla MPL2 license, and may not be monetized in any way, including but not
 * limited to selling the software or distributing it through ad-sponsored
 * channels.
 *
 * @author Jasper C. (Another Developer) <mail@another-developer.org>
 */

package org.another_developer.bukkitgui;

import org.another_developer.bukkitgui.tabs.backup.TabBackup;
import org.another_developer.bukkitgui.tabs.errorLogging.TabErrorLogging;
import org.another_developer.bukkitgui.tabs.general.TabGeneral;
import org.another_developer.bukkitgui.tabs.other.TabOther;
import org.another_developer.bukkitgui.tabs.players.TabPlayers;
import org.another_developer.bukkitgui.tabs.plugins.TabPlugins;
import org.another_developer.bukkitgui.tabs.serverOptions.TabServerOptions;
import org.another_developer.bukkitgui.tabs.superStart.TabSuperStart;
import org.another_developer.bukkitgui.tabs.taskManager.TabTaskManager;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.io.IOException;
import java.util.logging.Level;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import static org.another_developer.bukkitgui.BukkitGUI.getStackTrace;
import static org.another_developer.bukkitgui.BukkitGUI.log;
import static org.another_developer.bukkitgui.BukkitGUI.settings;

//FIXME: Window slightly resizes everytime it's oppened

public class BaseFrame extends JFrame {

    public static JTabbedPane baseTabs;

    private Image icon = null;
    private ImageIcon iconGeneral = new ImageIcon(getClass().getResource("/org/another_developer/bukkitgui/resources/information.png"));
    private ImageIcon iconPlayers = new ImageIcon(getClass().getResource("/org/another_developer/bukkitgui/resources/group.png"));
    private ImageIcon iconSuperStart = new ImageIcon(getClass().getResource("/org/another_developer/bukkitgui/resources/control_play.png"));
    private ImageIcon iconErrorLogging = new ImageIcon(getClass().getResource("/org/another_developer/bukkitgui/resources/error.png"));
    private ImageIcon iconTaskManager = new ImageIcon(getClass().getResource("/org/another_developer/bukkitgui/resources/clock.png"));
    private ImageIcon iconPlugins = new ImageIcon(getClass().getResource("/org/another_developer/bukkitgui/resources/plugin.png"));
    private ImageIcon iconServerOptions = new ImageIcon(getClass().getResource("/org/another_developer/bukkitgui/resources/wrench.png"));
    private ImageIcon iconBackup = new ImageIcon(getClass().getResource("/org/another_developer/bukkitgui/resources/backup.png"));
    private ImageIcon iconOther = new ImageIcon(getClass().getResource("/org/another_developer/bukkitgui/resources/settings.png"));

    private TabGeneral tabGeneral = new TabGeneral();
    private TabPlayers tabPlayers = new TabPlayers();
    private TabSuperStart tabSuperStart = new TabSuperStart();
    private TabErrorLogging tabErrorLogging = new TabErrorLogging();
    private TabTaskManager tabTaskManager = new TabTaskManager();
    private TabPlugins tabPlugins = new TabPlugins();
    private TabServerOptions tabServerOptions = new TabServerOptions();
    private TabBackup tabBackup = new TabBackup();
    private TabOther tabOther = new TabOther();

    private int width = 958, height = 550;

    public BaseFrame() {
        pack();

        for (int i = 0; i <= settings.size() - 1; i++) {
            String setting = settings.get(i);

            if (setting.startsWith("width=")) {
                String[] widthS = setting.split("=");

                width = Integer.parseInt(widthS[1]);
            }

            if (setting.startsWith("height=")) {
                String[] heightS = setting.split("=");

                height = Integer.parseInt(heightS[1]);
            }

            Insets insets = getInsets();
            setSize(new Dimension(insets.left + insets.right + width, insets.top + insets.bottom + height));
            setMinimumSize(new Dimension(insets.left + insets.right + 850, insets.top + insets.bottom + 525));
        }

        try {
            icon = ImageIO.read(getClass().getResourceAsStream("/org/another_developer/bukkitgui/resources/icon128.png"));
        } catch (IOException ex) {
            log.log(Level.WARNING, getStackTrace(ex));
        }

        log.log(Level.INFO, "BukkitGUI is running");

        setTitle("BukkitGUI - Test Preview");
        setIconImage(icon);
        setLocationRelativeTo(null);
        setVisible(true);
        
        addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                for (int i = 0; i <= settings.size() - 1; i++) {
                    if (settings.get(i).startsWith("width=")) {
                        settings.set(i, "width=" + getContentPane().getWidth());
                    }

                    if (settings.get(i).startsWith("height=")) {
                        settings.set(i, "height=" + getContentPane().getHeight());
                    }
                }
            }
        });

        addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {
                BukkitGUI.quit();
            }
        });

        initComponents();

        baseTabs.addChangeListener(new ChangeListener() {
            boolean superStart = false;

            @Override
            public void stateChanged(ChangeEvent e) {
                if (baseTabs.getSelectedIndex() == 2) {
                    superStart = true;
                }
                
                if (!(baseTabs.getSelectedIndex() == 2) && !(superStart == false)) {
                    tabSuperStart.updateArgs();
                    superStart = false;
                }
            }
        });
    }

    private void initComponents() {
        baseTabs = new JTabbedPane();

        baseTabs.addTab("General ", iconGeneral, tabGeneral);
        baseTabs.addTab("Players ", iconPlayers, tabPlayers);
        baseTabs.addTab("SuperStart ", iconSuperStart, tabSuperStart);
        baseTabs.addTab("Error Logging ", iconErrorLogging, tabErrorLogging);
        baseTabs.addTab("Task Manager ", iconTaskManager, tabTaskManager);
        baseTabs.addTab("Plugins ", iconPlugins, tabPlugins);
        baseTabs.addTab("Server Options ", iconServerOptions, tabServerOptions);
        baseTabs.addTab("Backup ", iconBackup, tabBackup);
        baseTabs.addTab("Options & Info ", iconOther, tabOther);

        getContentPane().add(baseTabs);
    }
}
