/**
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, you can
 * obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Source and compiled files may only be redistributed if they comply with the
 * mozilla MPL2 license, and may not be monetized in any way, including but not
 * limited to selling the software or distributing it through ad-sponsored
 * channels.
 *
 * @author Jasper C. (Another Developer) <mail@another-developer.org>
 */

package org.another_developer.bukkitgui;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BukkitGUI {

    protected static final String versionID = "0.0.1";
    private static String version = "";

    public static final String OS = System.getProperty("os.name");
    public static final String arch = System.getProperty("sun.arch.data.model");

    private static final DateFormat dateFormat = new SimpleDateFormat("-dd-MM-yyyy - hh-mm-ss");
    private static final Date date = new Date();

    private static final ADFormatter formatter = new ADFormatter();
    public static final Logger log = Logger.getLogger(BukkitGUI.class.getName());
    private static Handler handler;

    private static boolean installed = true;
    public static File appDir = null;
    private static File logs = null;
    public static File appExec = null;

    private static JFrame splash;
    private static JLabel background;
    private static JProgressBar progress;

    public static ArrayList<String> settings = new ArrayList<String>();

    /* <editor-fold>
     * The main method first off starts by detecting what OS you're running on and 
     * sets the application directories up if it hasn't been yet, and then proceeds 
     * to setting the paths.
     * 
     * After that we create our loading screen.
     * 
     * Then we download updates and create the application config. We continue with
     * parsing the config file and sending those vars to ArrayList String.
     * 
     * Now we close the splash and call BaseFrame to make out main GUI.
     *///</editor-fold>
    public static void main(String[] args) {
        if (OS.contains("Linux") || OS.contains("linux") || OS.contains("LINUX")) {
            appDir = new File(System.getProperty("user.home") + "/.bukkit_gui");
            logs = new File(appDir + File.separator + "logs");

            if (!appDir.exists()) {
                appDir.mkdir();
                logs.mkdir();

                installed = false;
            }
        } else if (OS.contains("Windows")) {
            appDir = new File(System.getProperty("user.home") + "\\AppData\\Roaming\\Bukkit_GUI");
            logs = new File(appDir + File.separator + "logs");

            if (!appDir.exists()) {
                appDir.mkdir();
                logs.mkdir();

                installed = false;
            }
        } else if (OS.contains("Mac OS X")) {
            appDir = new File(System.getProperty("user.home") + "/Library/Application Support/Bukkit_GUI");
            logs = new File(appDir + File.separator + "logs");

            if (!appDir.exists()) {
                appDir.mkdir();
                logs.mkdir();

                installed = false;
            }
        }

        try {
            log.setUseParentHandlers(false);

            handler = new FileHandler(logs + File.separator + "Log" + dateFormat.format(date) + ".txt");
            handler.setFormatter(formatter);

            log.addHandler(handler);

            log.log(Level.INFO, "BukkitGUI running on: " + OS + ", using version: " + versionID);

            try {
                for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                    UIManager.put("control", new Color(240, 240, 245));
                    UIManager.put("nimbusOrange", new Color(150, 150, 255));

                    if ("Nimbus".equals(info.getName())) {
                        UIManager.setLookAndFeel(info.getClassName());
                        break;
                    }
                }
            } catch (UnsupportedLookAndFeelException ex) {
                log.log(Level.WARNING, getStackTrace(ex));
            } catch (Exception ex) {
                log.log(Level.SEVERE, getStackTrace(ex));
            }
        } catch (Exception ex) {
            log.log(Level.SEVERE, getStackTrace(ex));
        }

        EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
                Image icon = null;
                ImageIcon imageLogo = null;
                ImageIcon imageBackground = null;

                try {
                    icon = ImageIO.read(getClass().getResourceAsStream("/org/another_developer/bukkitgui/resources/icon128.png"));
                    imageLogo = new ImageIcon(getClass().getResource("/org/another_developer/bukkitgui/resources/icon128.png"));
                    imageBackground = new ImageIcon(getClass().getResource("/org/another_developer/bukkitgui/resources/background.png"));
                } catch (Exception ex) {
                    log.log(Level.WARNING, getStackTrace(ex));
                }

                javax.swing.JOptionPane.showMessageDialog(null, "You're running a test release of BukkitGUI.\n"
                        + "This implies that the application could be\n"
                        + "unstable, bugged, and not optimized.\n\n"
                        + "Please report anything to the developer(s).\n"
                        + "You can contact me on the Minecraftforums\n"
                        + "and Bukkit as AnotherDeveloper.\n\n"//or at:\n"
//                        + "<html><b>mail@another-developer.org</b></html>\n\n"
                        + "Send the log as well, which is located here:\n"
                        + "<html><p><i>" + logs + "</i></p></html>", "Warning - Test release", JOptionPane.WARNING_MESSAGE, imageLogo);

                splash = new JFrame();
                background = new JLabel();
                progress = new JProgressBar();

                splash.setTitle("BukkitGUI - Test Preview");
                splash.setSize(560, 175);
                splash.setIconImage(icon);
                splash.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                splash.setUndecorated(true);
                splash.setLocationRelativeTo(null);
                splash.setAlwaysOnTop(true);
                splash.setResizable(false);
                splash.getContentPane().setLayout(null);
                splash.setVisible(true);

                progress.setString("Loading ...");
                progress.setStringPainted(true);
                splash.getContentPane().add(progress);
                progress.setBounds(5, 138, 550, 25);

                background.setIcon(imageBackground);
                splash.getContentPane().add(background);
                background.setBounds(0, 0, 560, 175);

                Runnable init;
                init = new Runnable() {

                    @Override
                    public void run() {
                        if (!installed) {
                            progress.setString("Installing BukkitGUI");
                            log.log(Level.INFO, "Installing BukkitGUI to system");

                            install();

                            installed = true;
                            progress.setString("BukkitGUI is installed");
                            log.log(Level.INFO, "BukkitGUI finished installing");
                        }

                        try {
                            progress.setString("Checking for update");
                            log.log(Level.INFO, "Checking for update");


                            URLConnection connection = new URL("https://dl.dropboxusercontent.com/u/30154867/BukkitGUI/version").openConnection();
                            connection.setConnectTimeout(60000);
                            connection.connect();

                            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

                            String inputLine;
                            while ((inputLine = in.readLine()) != null) {
                                version = version + inputLine;
                            }
                            in.close();

                            if (!version.equals(versionID)) {
                                String[] releaseCurrent = versionID.split("\\.");
                                int mainReleaseCurrent = Integer.parseInt(releaseCurrent[0]);
                                int majorReleaseCurrent = Integer.parseInt(releaseCurrent[1]);
                                int minorReleaseCurrent = Integer.parseInt(releaseCurrent[2]);

                                String[] release = version.split("\\.");
                                int mainRelease = Integer.parseInt(release[0]);
                                int majorRelease = Integer.parseInt(release[1]);
                                int minorRelease = Integer.parseInt(release[2]);

                                if (mainReleaseCurrent <= mainRelease) {
                                    if (majorReleaseCurrent <= majorRelease) {
                                        if (minorReleaseCurrent < minorRelease) {
                                            splash.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
                                            progress.setString("Update available ... fetching");
                                            log.log(Level.INFO, "A new version has been found: " + version);

                                            try {
                                                appExec = new File(BukkitGUI.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath());
                                            } catch (Exception ex) {
                                                log.log(Level.WARNING, getStackTrace(ex));
                                            }

                                            Download appFile = new Download(new URL("https://dl.dropboxusercontent.com/u/30154867/BukkitGUI/BukkitGUI-" + version + ".jar"), appExec.toString());

                                            try {
                                                while (true) {
                                                    switch (appFile.getStatus()) {
                                                        case 0:
                                                            progress.setMinimum(0);
                                                            while (appFile.getStatus() == 0) {
                                                                progress.setMaximum(appFile.getSize());
                                                                progress.setValue(appFile.getProgress());
                                                                progress.setString(Download.STATUSES[appFile.getStatus()] + " (" + appFile.getProgress() + "/" + appFile.getSize() + ")");
                                                                Thread.sleep(500);
                                                            }
                                                        case 1:
                                                            progress.setString(Download.STATUSES[appFile.getStatus()]);
                                                            Thread.sleep(1000);

                                                            if (progress.getString().equals("Complete")) {
                                                                Process process = Runtime.getRuntime().exec("java -jar " + appExec);
                                                                handler.close();
                                                                System.exit(0);
                                                            }
                                                        case 2:
                                                            UIManager.put("nimbusOrange", new Color(255, 50, 50));

                                                            progress.setMaximum(1);
                                                            progress.setValue(1);
                                                            progress.setString(Download.STATUSES[appFile.getStatus()]);

                                                            splash.setAlwaysOnTop(false);
                                                            JOptionPane.showMessageDialog(null, appFile.error, "An error has occured", JOptionPane.ERROR_MESSAGE);

                                                            log.log(Level.SEVERE, appFile.error);

                                                            handler.close();
                                                            System.exit(1);
                                                            break;
                                                    }
                                                    break;
                                                }
                                            } catch (Exception ex) {
                                                log.log(Level.SEVERE, getStackTrace(ex));
                                            }
                                        }
                                    }
                                }
                            }
                        } catch (UnknownHostException ex) {
                            log.log(Level.WARNING, getStackTrace(ex));
                        } catch (SocketTimeoutException ex) {
                            progress.setString("Connection timed out");
                            log.log(Level.WARNING, getStackTrace(ex));
                        } catch (Exception ex) {
                            log.log(Level.SEVERE, getStackTrace(ex));
                        }

                        try {
                            progress.setString("Intializing program");
                            log.log(Level.INFO, "Reading config");

                            BufferedReader in = new BufferedReader(new FileReader(new File(appDir + File.separator + "config.cfg")));
                            String inputLine;

                            while ((inputLine = in.readLine()) != null) {
                                if (inputLine.startsWith("<ui ") && inputLine.contains(" />")) {
                                    String[] settings = inputLine.replace("<ui ", "").replace(" />", "").split(" ");
                                    
                                    BukkitGUI.settings.addAll(Arrays.asList(settings));
                                }
                            }
                            in.close();
                            
                            if (installed) splash.dispose();

                            BaseFrame baseFrame = new BaseFrame();
                        } catch (Exception ex) {
                            log.log(Level.SEVERE, getStackTrace(ex));
                        }
                    }
                };
                Thread t1 = new Thread(init);
                t1.start();
            }
        });
    }

    /**
     * This method gets the stacktrace and puts it through a String builder to return
     * as a String. This is because logger is unable to handle exceptions.
     * 
     * @param throwable Gets the thrown exception
     * @return 
     */
    public static String getStackTrace(final Throwable throwable) {
        final StringWriter sw = new StringWriter();
        final PrintWriter pw = new PrintWriter(sw, true);
        throwable.printStackTrace(pw);

        return sw.getBuffer().toString();
    }

    /* <editor-fold>
     * This is called only from the main method to create the config file.
     *///</editor-fold>
    private static void install() {
        try {
            File config = new File(appDir + File.separator + "config.cfg");

            PrintWriter writer = new PrintWriter(new FileWriter(config));
            writer.println("<ui width=958 height=550 autoscroll=true say=false serverType=bukkit remote=false remoteType=SSH />\n"
                         + "<app version=" + versionID + " />\n");
            writer.close();
        } catch (Exception ex) {
            log.log(Level.SEVERE, getStackTrace(ex));
        }
    }

    /* <editor-fold>
     * This is called only by quit and takes all the entries from ArrayList settings 
     * and then rebuilds the config file.
     *///</editor-fold>
    private static void saveConfig() {
        if (!settings.isEmpty()) {
            try {
                log.log(Level.INFO, "Saving new config");
                File config = new File(appDir + File.separator + "config.cfg");

                PrintWriter writer = new PrintWriter(new FileWriter(config));
                for (int i = 0; i < settings.size() + 1; i++) {
                    if (i == 0) writer.print("<ui ");
                    if (i > 0 && i < settings.size() + 1) writer.print(settings.get(i - 1) + " ");
                    if (i == settings.size()) writer.println("/>\n<app version=" + versionID + " />");
                }
                writer.close();

                log.log(Level.INFO, "Config saved");
            } catch (Exception ex) {
                log.log(Level.SEVERE, getStackTrace(ex));
            }
        }
    }

    /**
     * Quit will stop the server (if one is even running), and save all settings,
     * then fully quits the application.
     */
    public static void quit() {
        Server.stopServer();
        Server.disconnectRemote();
        saveConfig();

        log.log(Level.OFF, "BukkitGUI has been closed by the user");
        handler.close();
        System.exit(0);
    }
}