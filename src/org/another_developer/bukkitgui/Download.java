/**
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, you can
 * obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Source and compiled files may only be redistributed if they comply with the
 * mozilla MPL2 license, and may not be monetized in any way, including but not
 * limited to selling the software or distributing it through ad-sponsored
 * channels.
 *
 * @author Jasper C. (Another Developer) <mail@another-developer.org>
 */

package org.another_developer.bukkitgui;

import java.awt.Color;
import java.io.File;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.io.StringWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.Observable;
import java.util.logging.Level;
import javax.swing.*;
import static org.another_developer.bukkitgui.BukkitGUI.getStackTrace;
import static org.another_developer.bukkitgui.BukkitGUI.log;

public class Download extends Observable implements Runnable {

    private static final int MAX_BUFFER_SIZE = 1024;

    public static final String STATUSES[] = {"Downloading", "Complete", "Error"};

    public static final int DOWNLOADING = 0;
    public static final int COMPLETE = 1;
    public static final int ERROR = 2;

    private final URL url;
    private int size;
    private int downloaded;
    private int status;

    private final String outputFile;
    protected String error;
    private boolean replace = true;

    /**
     * Sets variables for downloading and then calls download() to create a new thread
     * and download the needed file.
     * 
     * @param url URL to file that needs to be downloaded
     * @param destination Destination for the file being downloaded
     */
    public Download(URL url, String destination) {
        this.url = url;
        size = -1;
        downloaded = 0;
        status = DOWNLOADING;
        outputFile = destination;

        download();
    }

    /**
     * Sets variables for downloading and then calls download() to create a new thread
     * and download the needed file.
     * 
     * @param url URL to file that needs to be downloaded
     * @param destination Destination for the file being downloaded
     * @param gui If returned true, a gui will be made displaying download status
     * @param title Sets the title of the dialog
     */
    Download(URL url, String destination, boolean gui, String title) {
        this.url = url;
        size = -1;
        downloaded = 0;
        replace = false;
        status = DOWNLOADING;
        outputFile = destination;
        //FIXME: Gettinf a null pointer because no path was set

        new DownloadDialog(title);
        download();
    }

    /**
     * Returns the URL for the file that is being downloaded.
     * 
     * @return 
     */
    public String getUrl() {
        return url.toString();
    }

    /**
     * Returns the size of the file in bytes.
     * 
     * @return 
     */
    public int getSize() {
        return size;
    }

    /**
     * Returns how many bytes have been downloaded.
     * 
     * @return 
     */
    public int getProgress() {
        return downloaded;
    }

    /**
     * Returns the status index. This number can be used to obtain a string from STATUSES[]
     * 
     * DOWNLOADING = 0;
     * COMPLETE = 1;
     * ERROR = 2;
     * 
     * @return
     */
    public int getStatus() {
        return status;
    }

    /* <editor-fold>
     * Calls a state change if in error has been reached.
     *///</editor-fold>
    private void error() {
        status = ERROR;
        stateChanged();
    }

    /* <editor-fold>
     * Calls a state change if in error has been reached, and sends the exception
     * to a String.
     *///</editor-fold>
    private void error(Exception ex) {
        StringWriter errors = new StringWriter();
        ex.printStackTrace(new PrintWriter(errors));
        error = errors.toString();

        status = ERROR;
        stateChanged();
    }

    /* <editor-fold>
     * Creates a thread for downloading the needed file.
     *///</editor-fold>
    private void download() {
        Thread thread = new Thread(this);
        thread.start();
    }

    /* <editor-fold>
     * Gets the name of the file being downloaded.
     *///</editor-fold>
    private String getFileName(URL url) {
        String fileName = url.getFile();
        return fileName.substring(fileName.lastIndexOf('/') + 1);
    }

    /* <editor-fold>
     * Gets file size and downloads the file to it destination. If problems occur, 
     * exceptions will be called and the download will fail. When done, all streams
     * get closed.
     *///</editor-fold>
    @Override
    public void run() {
        RandomAccessFile file = null;
        InputStream stream = null;

        try {
            URLConnection connection = (URLConnection) url.openConnection();
            connection.setRequestProperty("Range", "bytes=" + downloaded + "-");
            connection.connect();

            int contentLength = connection.getContentLength();
            if (contentLength < 1) {
                error();
            }

            if (size == -1) {
                size = contentLength;
                stateChanged();
            }

            if (replace) {
                file = new RandomAccessFile(outputFile, "rw");
                file.seek(downloaded);
            } else if (!replace) {
                file = new RandomAccessFile(outputFile + getFileName(url), "rw");
                file.seek(downloaded);
            }

            stream = connection.getInputStream();
            while (status == DOWNLOADING) {
                byte buffer[];
                if (size - downloaded > MAX_BUFFER_SIZE) {
                    buffer = new byte[MAX_BUFFER_SIZE];
                } else {
                    buffer = new byte[size - downloaded];
                }

                int read = stream.read(buffer);
                if (read == -1)
                    break;

                file.write(buffer, 0, read);
                downloaded += read;
                stateChanged();
            }

            if (status == DOWNLOADING) {
                status = COMPLETE;
                stateChanged();
            }
        } catch (Exception ex) {
            error(ex);
        } finally {
            if (file != null) {
                try {
                    file.close();
                } catch (Exception e) {
                }
            }

            if (stream != null) {
                try {
                    stream.close();
                } catch (Exception e) {
                }
            }
        }
    }

    /* <editor-fold>
     * Updates state for Observable.
     *///</editor-fold>
    private void stateChanged() {
        setChanged();
        notifyObservers();
    }

    private class DownloadDialog {

        private final JDialog dowloadDialog = new JDialog();
        private JLabel download;
        private JLabel downloadLabel;
        private JButton closeButton;
        private JButton cancelButton;
        private JProgressBar downloadProgress;
        private JLabel serverTpyeLabel;
        private JLabel serverType;
        private JLabel version;
        private JLabel versionLabel;
        
        public DownloadDialog(String title) {
            initComponents();
            
            //TODO: Set label text arguments
            download.setText(url.toString());
            
            dowloadDialog.setTitle(title);
            dowloadDialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            dowloadDialog.setResizable(false);
            dowloadDialog.setAlwaysOnTop(true);
            dowloadDialog.pack();
            dowloadDialog.setLocationRelativeTo(null);
            dowloadDialog.setVisible(true);
            
            loop();
        }
                        
        private void initComponents() {
            closeButton = new JButton();
            cancelButton = new JButton();
            downloadProgress = new JProgressBar();
            serverTpyeLabel = new JLabel();
            downloadLabel = new JLabel();
            versionLabel = new JLabel();
            serverType = new JLabel();
            version = new JLabel();
            download = new JLabel();

            closeButton.setText("Close");
            closeButton.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    dowloadDialog.dispose();
                }
            });

            cancelButton.setText("Cancel");
            cancelButton.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent e) {
                    //TODO: Stop download
                }
            });

            downloadProgress.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
            downloadProgress.setString("");
            downloadProgress.setStringPainted(true);

            serverTpyeLabel.setText("Server Type:");

            downloadLabel.setText("Downloading from:");

            versionLabel.setText("Version:");

            serverType.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

            version.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

            download.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

            GroupLayout layout = new GroupLayout(dowloadDialog.getContentPane());
            dowloadDialog.getContentPane().setLayout(layout);
            layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(downloadProgress, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                            .addGap(0, 0, Short.MAX_VALUE)
                            .addComponent(cancelButton)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(closeButton))
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(downloadLabel)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(download, GroupLayout.DEFAULT_SIZE, 236, Short.MAX_VALUE))
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(serverTpyeLabel)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(serverType, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(versionLabel)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(version, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addContainerGap())
            );
            layout.setVerticalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(serverTpyeLabel)
                        .addComponent(serverType))
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(versionLabel)
                        .addComponent(version))
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(downloadLabel)
                        .addComponent(download))
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(downloadProgress, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(closeButton)
                        .addComponent(cancelButton))
                    .addContainerGap())
            );
        }
        
        private void loop() {
            Runnable loop = new Runnable() {

                @Override
                public void run() {
                    try {
                        while (true) {
                            switch (getStatus()) {
                                case 0:
                                    downloadProgress.setMinimum(0);
                                    while (getStatus() == 0) {
                                        downloadProgress.setMaximum(getSize());
                                        downloadProgress.setValue(getProgress());
                                        downloadProgress.setString(Download.STATUSES[getStatus()] + " (" + getProgress() + "/" + getSize() + ")");
                                        Thread.sleep(500);
                                    }
                                case 1:
                                    downloadProgress.setString(Download.STATUSES[getStatus()]);
                                    break;
                                case 2:
                                    downloadProgress.setBackground(Color.red);
                                    downloadProgress.setMinimum(0);
                                    downloadProgress.setMaximum(1);
                                    downloadProgress.setValue(1);

                                    dowloadDialog.setAlwaysOnTop(false);
                                    downloadProgress.setString("Error: " + error);

                                    log.log(Level.SEVERE, error);
                                    break;
                            }
                            break;
                        }
                    } catch (Exception ex) {
                        log.log(Level.SEVERE, getStackTrace(ex));
                    }
                }
            };
            Thread t1 = new Thread(loop);
            t1.start();
        }
    }
}