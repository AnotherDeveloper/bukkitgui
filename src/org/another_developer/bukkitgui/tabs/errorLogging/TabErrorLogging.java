/**
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, you can
 * obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Source and compiled files may only be redistributed if they comply with the
 * mozilla MPL2 license, and may not be monetized in any way, including but not
 * limited to selling the software or distributing it through ad-sponsored
 * channels.
 *
 * @author Jasper C. (Another Developer) <mail@another-developer.org>
 */

package org.another_developer.bukkitgui.tabs.errorLogging;

public class TabErrorLogging extends javax.swing.JPanel {

    public TabErrorLogging() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Message = new javax.swing.JLabel();

        setLayout(new java.awt.BorderLayout());

        Message.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Message.setText("This tab hasn't been fully created yet. It will be added in a future patch.");
        add(Message, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Message;
    // End of variables declaration//GEN-END:variables
}
