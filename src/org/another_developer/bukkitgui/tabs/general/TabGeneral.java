/**
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, you can
 * obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Source and compiled files may only be redistributed if they comply with the
 * mozilla MPL2 license, and may not be monetized in any way, including but not
 * limited to selling the software or distributing it through ad-sponsored
 * channels.
 *
 * @author Jasper C. (Another Developer) <mail@another-developer.org>
 */

package org.another_developer.bukkitgui.tabs.general;

import com.sun.management.OperatingSystemMXBean;
import java.awt.KeyboardFocusManager;
import org.another_developer.bukkitgui.Server;
import javax.swing.*;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.Collections;
import java.util.logging.Level;
import javax.swing.table.DefaultTableModel;

import static org.another_developer.bukkitgui.BukkitGUI.getStackTrace;
import static org.another_developer.bukkitgui.BukkitGUI.log;
import static org.another_developer.bukkitgui.BukkitGUI.settings;

//TODO: User head: http://skins.minecraft.net/MinecraftSkins/<USERNAME>.png
//TODO: Add command auto-complete

public class TabGeneral extends JPanel {

    private static String output = "";
    private static ArrayList<String> users = new ArrayList<String>();
    
    private final String[] vanillaCommands = new String[] {"/achievement", "/ban", "/ban-ip", "/banlist", "/blockdata", "/clear", "/clone", "/debug", 
        "/defaultgamemode", "/deop", "/difficulty", "/effect", "/enchant", "/entitydata", "/execute", "/fill", "/gamemode", "/gamerule", "/give", "/help", 
        "/kick", "/kill", "/list", "/me", "/op", "/pardon", "/particle", "/playsound", "/publish", "/replaceitem", "/save-all", "/save-off", "/save-on", 
        "/say", "/scoreboard", "/seed", "/setblock", "/setidletimeout", "/setworldspawn", "/spawnpoint", "/spreadplayers", "/stats", "/stop", "/summon", 
        "/tell", "/tellraw", "/testfor", "/testforblock", "/testforblocks", "/time", "/title", "/toggledownfall", "/tp", "/trigger", "/weather", "/whitelist", 
        "/worldborder", "/xp"
    };
    private String serverInputString;
    private int serverInputInt;
    
    private boolean scroll;
    
    private DefaultTableModel playerModel;
    private static int userAmountA;
    private int userAmountB;

    /* <editor-fold>
     * Initializes the class in their needed order.
     *///</editor-fold>
    public TabGeneral() {
        initComponents();
        postInit();
        loop();
    }

    /* <editor-fold>
     * Parses application wide settings and applies them.
     *///</editor-fold>
    private void postInit() {
        for (int i = 0; i <= settings.size() - 1; i++) {
            String setting = settings.get(i);

            if (setting.startsWith("say")) {
                String[] say = setting.split("=");

                if (say[1].equals("true")) {
                    sayCheckBox.setSelected(true);
                }
            }

            if (setting.startsWith("autoscroll")) {
                String[] autoscroll = setting.split("=");

                if (autoscroll[1].equals("true")) {
                    autoscrollCheckBox.setSelected(true);
                }
            }
        }
        
        setFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, Collections.EMPTY_SET);
        
        outputScrollPane.getVerticalScrollBar().addMouseListener(new MouseAdapter() {

            @Override
            public void mousePressed(MouseEvent e) {
                System.err.println("Pressed");
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                System.err.println("Released");
            }
        });

        outputScrollPane.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {

            @Override
            public void adjustmentValueChanged(AdjustmentEvent e) {
                //FIXME: Scroll either wont disengage, or else disengage when it doesn't need to
                //Use mouse listner to temporarly disable autoscroll
                if (autoscrollCheckBox.isSelected() && scroll) {
                    e.getAdjustable().setValue(e.getAdjustable().getMaximum());
                }

                if (e.getAdjustable().getValue() + outputScrollPane.getVerticalScrollBar().getModel().getExtent() != e.getAdjustable().getMaximum()) {
                    scroll = false;
                }

                if (e.getAdjustable().getValue() + outputScrollPane.getVerticalScrollBar().getModel().getExtent() == e.getAdjustable().getMaximum()) {
                    scroll = true;
                }
            }
        });
    }

    /* <editor-fold>
     * GUI loop updates resource ussage and server output.
     *///</editor-fold>
    private void loop() {
        Runnable loop = new Runnable() {

            @Override
            public void run() {
                int maxMemory = (int) ((((OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean()).getTotalPhysicalMemorySize() / 1024) / 1024);
                int percentTotalMemory, percentGUIMemory, percentServerMemory;

                ramGUI.setMaximum(maxMemory);
                ramServer.setMaximum(maxMemory);
                ramTotal.setMaximum(maxMemory);

                while (true) {
                    try {
                        cpuTotal.setValue((int) (((OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean()).getSystemCpuLoad() * 100));
                        cpuTotal.setString(cpuTotal.getValue() + "%");

                        //TODO: Add server CPU usage

                        cpuGUI.setValue((int) (((OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean()).getProcessCpuLoad() * 100));
                        cpuGUI.setString(cpuGUI.getValue() + "%");

                        percentTotalMemory = (int) (((double) ramTotal.getValue() / maxMemory) * 100.0);
                        percentGUIMemory = (int) (((double) ramGUI.getValue() / maxMemory) * 100.0);

                        //FIXME: RAM total gets shows more mem use than what really is
                        ramTotal.setValue(maxMemory - ((int) ((((OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean()).getFreePhysicalMemorySize() / 1024) / 1024)));
                        ramTotal.setString(ramTotal.getValue() + "Mb (" + percentTotalMemory + "%)");

                        //TODO: Add server RAM usage

                        //FIXME: Doesn't include extra JVM bloat
                        ramGUI.setValue((int) (((ManagementFactory.getMemoryMXBean().getHeapMemoryUsage().getUsed()) / 1024) / 1024));
                        ramGUI.setString(ramGUI.getValue() + "Mb (" + percentGUIMemory + "%)");

                        appendOutput();     Thread.sleep(100);
                        appendOutput();     Thread.sleep(100);
                        appendOutput();     Thread.sleep(100);
                        appendOutput();     Thread.sleep(100);
                        appendOutput();     Thread.sleep(100);
                    } catch (Exception ex) {
                        log.log(Level.SEVERE, getStackTrace(ex));
                    }
                }
            }
            
            private void appendOutput() {
                playerModel = (DefaultTableModel) playerTable.getModel();
                
                try {
                    //TODO: Add users face and be a bit more efficient by not recreating the model and table

                    if (!output.equals("")) {
                        if(output.contains("Starting local server: ") || output.contains("Starting remote server: ")) outputTextPane.setText("");
                        outputTextPane.getDocument().insertString(outputTextPane.getDocument().getLength(), output + "\n", null);
                        output = "";
                    }

                    if (userAmountA > userAmountB) {
                        for (String user : users) {
                            String player[] = user.split(" ");

                            playerModel.addRow(new Object[] {player[0]});
                        }

                        userAmountB = userAmountA;
                    }

                    if (userAmountA < userAmountB) {
                        for (String user : users) {
                            int index = users.indexOf(user);

                            if (user.equals("")) {
                                playerModel.removeRow(index);
                                users.trimToSize();
                            }
                        }

                        if (users.isEmpty()) {
                            playerModel.removeRow(0);
                        }

                        userAmountB = userAmountA;
                    }
                } catch (Exception ex) {
                    log.log(Level.SEVERE, getStackTrace(ex));
                }
            }
        };
        Thread t1 = new Thread(loop);
        t1.start();
    }

    /**
     * Buffers and appends output for the GUI loop. As well parses the output to detect
     * if certain actions take place, like players joining.
     * 
     * @param output Server output
     */
    public static void appendToOutput(String output) {
        if (output.contains(": UUID of player")) {
            String[] user = output.replace("] [User Authenticator ", "").replace(": UUID of player", "").replace(" is", "").split(" ");
            users.add(user[1] + " " + user[2]);
            
            userAmountA++;
        }

        if (output.contains("left the game")) {
            String userLeft[] = output.replace("] [Server thread/INFO", "").replace(" left the game", "").split(" ");
                
            for (int i = 0; i < users.size(); i++) {
                String user[] = users.get(i).split(" ");
                
                if (userLeft[1].equals(user[0])) {
                    users.remove(i);
                    
                    userAmountA--;
                }
            }
        }
        
        if (!TabGeneral.output.equals("")) {
            TabGeneral.output = TabGeneral.output + "\n" + output;
        } else if (TabGeneral.output.equals("")){
            TabGeneral.output = output;
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        serverPanel = new javax.swing.JPanel();
        sendButton = new javax.swing.JButton();
        playerScrollPane = new javax.swing.JScrollPane();
        playerTable = new javax.swing.JTable();
        serverInput = new javax.swing.JTextField();
        sayCheckBox = new javax.swing.JCheckBox();
        autoscrollCheckBox = new javax.swing.JCheckBox();
        outputScrollPane = new javax.swing.JScrollPane();
        outputTextPane = new javax.swing.JTextPane();
        performancePanel = new javax.swing.JPanel();
        cpuGUILabel = new javax.swing.JLabel();
        cpuServerLabel = new javax.swing.JLabel();
        cpuTotalLabel = new javax.swing.JLabel();
        cpuTotal = new javax.swing.JProgressBar();
        cpuServer = new javax.swing.JProgressBar();
        cpuGUI = new javax.swing.JProgressBar();
        ramGUILabel = new javax.swing.JLabel();
        ramServerLabel = new javax.swing.JLabel();
        ramTotalLabel = new javax.swing.JLabel();
        ramTotal = new javax.swing.JProgressBar();
        ramServer = new javax.swing.JProgressBar();
        ramGUI = new javax.swing.JProgressBar();
        usageCPULabel = new javax.swing.JLabel();
        usageRAMLabel = new javax.swing.JLabel();
        filler2 = new javax.swing.Box.Filler(new java.awt.Dimension(25, 0), new java.awt.Dimension(25, 0), new java.awt.Dimension(25, 32767));
        quickActionPanel = new javax.swing.JPanel();
        stopButton = new javax.swing.JButton();
        reloadButton = new javax.swing.JButton();
        restartButton = new javax.swing.JButton();
        startButton = new javax.swing.JButton();
        killButton = new javax.swing.JButton();

        serverPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Server Management", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 0, 11))); // NOI18N

        sendButton.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        sendButton.setText("Send");
        sendButton.setMaximumSize(new java.awt.Dimension(80, 25));
        sendButton.setMinimumSize(new java.awt.Dimension(80, 25));
        sendButton.setPreferredSize(new java.awt.Dimension(80, 25));
        sendButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sendButtonActionPerformed(evt);
            }
        });

        playerScrollPane.setPreferredSize(new java.awt.Dimension(80, 80));

        playerTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Name"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        playerScrollPane.setViewportView(playerTable);
        if (playerTable.getColumnModel().getColumnCount() > 0) {
            playerTable.getColumnModel().getColumn(0).setResizable(false);
        }

        serverInput.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                serverInputKeyPressed(evt);
            }
        });

        sayCheckBox.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        sayCheckBox.setText("Say (Ctrl+S)");
        sayCheckBox.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        sayCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sayCheckBoxActionPerformed(evt);
            }
        });

        autoscrollCheckBox.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        autoscrollCheckBox.setText("Auto Scroll");
        autoscrollCheckBox.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        autoscrollCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                autoscrollCheckBoxActionPerformed(evt);
            }
        });

        outputTextPane.setEditable(false);
        outputScrollPane.setViewportView(outputTextPane);

        javax.swing.GroupLayout serverPanelLayout = new javax.swing.GroupLayout(serverPanel);
        serverPanel.setLayout(serverPanelLayout);
        serverPanelLayout.setHorizontalGroup(
            serverPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(serverPanelLayout.createSequentialGroup()
                .addGroup(serverPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(playerScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(sayCheckBox)
                    .addComponent(autoscrollCheckBox))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(serverPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(serverPanelLayout.createSequentialGroup()
                        .addComponent(serverInput)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(sendButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(outputScrollPane)))
        );
        serverPanelLayout.setVerticalGroup(
            serverPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, serverPanelLayout.createSequentialGroup()
                .addComponent(playerScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 363, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(autoscrollCheckBox)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(sayCheckBox))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, serverPanelLayout.createSequentialGroup()
                .addComponent(outputScrollPane)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(serverPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(sendButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(serverInput, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        performancePanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "System Resources", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 0, 11))); // NOI18N

        cpuGUILabel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        cpuGUILabel.setText("GUI:");

        cpuServerLabel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        cpuServerLabel.setText("Server:");

        cpuTotalLabel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        cpuTotalLabel.setText("Total:");

        cpuTotal.setFont(new java.awt.Font("Dialog", 0, 11)); // NOI18N
        cpuTotal.setForeground(new java.awt.Color(0, 0, 0));
        cpuTotal.setString("");
        cpuTotal.setStringPainted(true);

        cpuServer.setFont(new java.awt.Font("Dialog", 0, 11)); // NOI18N
        cpuServer.setForeground(new java.awt.Color(0, 0, 0));
        cpuServer.setString("");
        cpuServer.setStringPainted(true);

        cpuGUI.setFont(new java.awt.Font("Dialog", 0, 11)); // NOI18N
        cpuGUI.setForeground(new java.awt.Color(0, 0, 0));
        cpuGUI.setString("");
        cpuGUI.setStringPainted(true);

        ramGUILabel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        ramGUILabel.setText("GUI:");

        ramServerLabel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        ramServerLabel.setText("Server:");

        ramTotalLabel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        ramTotalLabel.setText("Total:");

        ramTotal.setFont(new java.awt.Font("Dialog", 0, 11)); // NOI18N
        ramTotal.setForeground(new java.awt.Color(0, 0, 0));
        ramTotal.setMaximum(0);
        ramTotal.setString("");
        ramTotal.setStringPainted(true);

        ramServer.setFont(new java.awt.Font("Dialog", 0, 11)); // NOI18N
        ramServer.setForeground(new java.awt.Color(0, 0, 0));
        ramServer.setMaximum(0);
        ramServer.setString("");
        ramServer.setStringPainted(true);

        ramGUI.setFont(new java.awt.Font("Dialog", 0, 11)); // NOI18N
        ramGUI.setForeground(new java.awt.Color(0, 0, 0));
        ramGUI.setMaximum(0);
        ramGUI.setString("");
        ramGUI.setStringPainted(true);

        usageCPULabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        usageCPULabel.setText("CPU Usage");

        usageRAMLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        usageRAMLabel.setText("RAM Usage");

        javax.swing.GroupLayout performancePanelLayout = new javax.swing.GroupLayout(performancePanel);
        performancePanel.setLayout(performancePanelLayout);
        performancePanelLayout.setHorizontalGroup(
            performancePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(performancePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(performancePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(performancePanelLayout.createSequentialGroup()
                        .addComponent(cpuTotalLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cpuTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(performancePanelLayout.createSequentialGroup()
                        .addGroup(performancePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(cpuGUILabel)
                            .addComponent(cpuServerLabel))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(performancePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(cpuServer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cpuGUI, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(usageCPULabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(filler2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(performancePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(performancePanelLayout.createSequentialGroup()
                        .addComponent(ramTotalLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ramTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(usageRAMLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(performancePanelLayout.createSequentialGroup()
                        .addComponent(ramGUILabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ramGUI, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(performancePanelLayout.createSequentialGroup()
                        .addComponent(ramServerLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ramServer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(257, Short.MAX_VALUE))
        );
        performancePanelLayout.setVerticalGroup(
            performancePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(performancePanelLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addGroup(performancePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(filler2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, performancePanelLayout.createSequentialGroup()
                        .addComponent(usageRAMLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(performancePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(ramGUILabel)
                            .addComponent(ramGUI, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(performancePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(ramServerLabel)
                            .addComponent(ramServer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(performancePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(ramTotalLabel)
                            .addComponent(ramTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, performancePanelLayout.createSequentialGroup()
                        .addComponent(usageCPULabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(performancePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(cpuGUILabel)
                            .addComponent(cpuGUI, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(performancePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(cpuServer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cpuServerLabel))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(performancePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cpuTotalLabel, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(cpuTotal, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
        );

        quickActionPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Quick Action", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 0, 11))); // NOI18N

        stopButton.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        stopButton.setText("Stop");
        stopButton.setMaximumSize(new java.awt.Dimension(100, 25));
        stopButton.setMinimumSize(new java.awt.Dimension(100, 25));
        stopButton.setPreferredSize(new java.awt.Dimension(100, 25));
        stopButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stopButtonActionPerformed(evt);
            }
        });

        reloadButton.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        reloadButton.setText("Reload");
        reloadButton.setMaximumSize(new java.awt.Dimension(100, 25));
        reloadButton.setMinimumSize(new java.awt.Dimension(100, 25));
        reloadButton.setPreferredSize(new java.awt.Dimension(100, 25));
        reloadButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                reloadButtonActionPerformed(evt);
            }
        });

        restartButton.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        restartButton.setText("Restart");
        restartButton.setMaximumSize(new java.awt.Dimension(100, 25));
        restartButton.setMinimumSize(new java.awt.Dimension(100, 25));
        restartButton.setPreferredSize(new java.awt.Dimension(100, 25));
        restartButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                restartButtonActionPerformed(evt);
            }
        });

        startButton.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        startButton.setText("Start");
        startButton.setMaximumSize(new java.awt.Dimension(100, 25));
        startButton.setMinimumSize(new java.awt.Dimension(100, 25));
        startButton.setPreferredSize(new java.awt.Dimension(100, 25));
        startButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startButtonActionPerformed(evt);
            }
        });

        killButton.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        killButton.setText("Kill");
        killButton.setMaximumSize(new java.awt.Dimension(100, 25));
        killButton.setMinimumSize(new java.awt.Dimension(100, 25));
        killButton.setPreferredSize(new java.awt.Dimension(100, 25));
        killButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                killButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout quickActionPanelLayout = new javax.swing.GroupLayout(quickActionPanel);
        quickActionPanel.setLayout(quickActionPanelLayout);
        quickActionPanelLayout.setHorizontalGroup(
            quickActionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(quickActionPanelLayout.createSequentialGroup()
                .addGroup(quickActionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(restartButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(startButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(quickActionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(stopButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(killButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
            .addComponent(reloadButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        quickActionPanelLayout.setVerticalGroup(
            quickActionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(quickActionPanelLayout.createSequentialGroup()
                .addGroup(quickActionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(stopButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(startButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(quickActionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(restartButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(killButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(reloadButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(serverPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addComponent(quickActionPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(performancePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(serverPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(performancePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(quickActionPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void serverInputKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_serverInputKeyPressed
        if (sayCheckBox.isSelected()) {
            if (evt.getKeyCode() == KeyEvent.VK_S && evt.isControlDown()) {
                if (!serverInput.getText().startsWith("/say ")) serverInput.setText("/say " + serverInput.getText());
            }
        }

        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            Server.sendCommandToServer(serverInput.getText());

            serverInput.setText("");
        }
        
        //TODO: If tab is pressed again, cycle through others that also start with
        if (evt.getKeyCode() == KeyEvent.VK_TAB) {
            String[] command = serverInput.getText().split(" ");
            serverInputString = command[0];
            
            for (String vanillaCommand : vanillaCommands) {
                if (vanillaCommand.startsWith(command[0]) && !serverInput.getText().isEmpty()) {
                    command[0] = vanillaCommand;
                    for (String input : command) {
                        serverInput.setText(input);
                        serverInput.grabFocus();
                    }
                }
            }
        }
    }//GEN-LAST:event_serverInputKeyPressed

    private void startButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_startButtonActionPerformed
        Server.startServer(Server.location);
    }//GEN-LAST:event_startButtonActionPerformed

    private void stopButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_stopButtonActionPerformed
        Server.stopServer();
    }//GEN-LAST:event_stopButtonActionPerformed

    private void restartButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_restartButtonActionPerformed
        Server.restartServer();
    }//GEN-LAST:event_restartButtonActionPerformed

    private void killButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_killButtonActionPerformed
        Server.killServer();
    }//GEN-LAST:event_killButtonActionPerformed

    private void reloadButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_reloadButtonActionPerformed
        Server.reloadServer();
    }//GEN-LAST:event_reloadButtonActionPerformed

    private void sendButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sendButtonActionPerformed
        Server.sendCommandToServer(serverInput.getText());
    }//GEN-LAST:event_sendButtonActionPerformed

    private void autoscrollCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_autoscrollCheckBoxActionPerformed
        for (int i = 0; i <= settings.size() - 1; i++) {
            if (settings.get(i).startsWith("autosroll=")) {
                settings.set(i, "autoscroll=" + autoscrollCheckBox.isSelected());
            }
        }
    }//GEN-LAST:event_autoscrollCheckBoxActionPerformed

    private void sayCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sayCheckBoxActionPerformed
        for (int i = 0; i <= settings.size() - 1; i++) {
            if (settings.get(i).startsWith("say=")) {
                settings.set(i, "say=" + sayCheckBox.isSelected());
            }
        }
    }//GEN-LAST:event_sayCheckBoxActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox autoscrollCheckBox;
    private javax.swing.JProgressBar cpuGUI;
    private javax.swing.JLabel cpuGUILabel;
    private javax.swing.JProgressBar cpuServer;
    private javax.swing.JLabel cpuServerLabel;
    private javax.swing.JProgressBar cpuTotal;
    private javax.swing.JLabel cpuTotalLabel;
    private javax.swing.Box.Filler filler2;
    private javax.swing.JButton killButton;
    private javax.swing.JScrollPane outputScrollPane;
    private javax.swing.JTextPane outputTextPane;
    private javax.swing.JPanel performancePanel;
    private javax.swing.JScrollPane playerScrollPane;
    private javax.swing.JTable playerTable;
    private javax.swing.JPanel quickActionPanel;
    private javax.swing.JProgressBar ramGUI;
    private javax.swing.JLabel ramGUILabel;
    private javax.swing.JProgressBar ramServer;
    private javax.swing.JLabel ramServerLabel;
    private javax.swing.JProgressBar ramTotal;
    private javax.swing.JLabel ramTotalLabel;
    private javax.swing.JButton reloadButton;
    private javax.swing.JButton restartButton;
    private javax.swing.JCheckBox sayCheckBox;
    private javax.swing.JButton sendButton;
    private javax.swing.JTextField serverInput;
    private javax.swing.JPanel serverPanel;
    private javax.swing.JButton startButton;
    private javax.swing.JButton stopButton;
    private javax.swing.JLabel usageCPULabel;
    private javax.swing.JLabel usageRAMLabel;
    // End of variables declaration//GEN-END:variables
}