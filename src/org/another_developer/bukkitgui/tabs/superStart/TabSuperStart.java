/**
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, you can
 * obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Source and compiled files may only be redistributed if they comply with the
 * mozilla MPL2 license, and may not be monetized in any way, including but not
 * limited to selling the software or distributing it through ad-sponsored
 * channels.
 *
 * @author Jasper C. (Another Developer) <mail@another-developer.org>
 */

package org.another_developer.bukkitgui.tabs.superStart;

import com.sun.management.OperatingSystemMXBean;
import org.another_developer.bukkitgui.BaseFrame;
import org.another_developer.bukkitgui.BukkitGUI;
import org.another_developer.bukkitgui.Server;
import org.another_developer.bukkitgui.WinRegistry;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.lang.management.ManagementFactory;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;

import static org.another_developer.bukkitgui.BukkitGUI.getStackTrace;
import static org.another_developer.bukkitgui.BukkitGUI.log;
import static org.another_developer.bukkitgui.BukkitGUI.settings;

public class TabSuperStart extends javax.swing.JPanel {

    private String argsMin = "", argsMax = "";
    private ArrayList<String> vanillaVersions = new ArrayList<String>();
    private ArrayList<String> vanillaSnapshots = new ArrayList<String>();

    /* <editor-fold>
     * Intializes this tab and if the tab is no longer in focus, it will update the
     * server based arguments.
     *///</editor-fold>
    public TabSuperStart() {
        initComponents();
        postInit();
    }

    private boolean ranOnce = false;
    
    /* <editor-fold>
     * Parses application wide settings and applies them.
     *///</editor-fold>
    private void postInit() {
        for (int i = 0; i <= settings.size() - 1; i++) {
            String setting = settings.get(i);

            if (setting.startsWith("remote=")) {
                String[] remote = setting.split("=");
                if (remote[1].equals("true")) remoteCheckBox.setSelected(true);
                if (remote[1].equals("false")) remoteCheckBox.setSelected(false);
            }

            //TODO: Needs to load args from settins
            if ((int) ((((OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean()).getTotalPhysicalMemorySize() / 1024) / 1024) > 1024) {
                minRAMSlider.setValue(1024);
                customArgumentsTextField.setText("-Xms1024M -Xmx1024M ");
            }

            if (setting.startsWith("serverType=")) {
                if (!ranOnce) {
                    try {
                        //TODO: Get url based off of selected server type
                        URLConnection connection = new URL("https://dl.dropboxusercontent.com/u/30154867/BukkitGUI/Vanilla").openConnection();
                        connection.setConnectTimeout(60000);
                        connection.connect();

                        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                        String inputLine;

                        while ((inputLine = in.readLine()) != null) {
                            vanillaVersions.add(inputLine);
                        }
                        in.close();
                    } catch (Exception ex) {
                        log.log(Level.WARNING, getStackTrace(ex));
                    }

                    ranOnce = true;
                }
                
                //TODO: for-loop needs to be able to load version for other server types. Move up to switch statement
                for (String data : vanillaVersions) {
                    String version[] = data.split(": ");
                    serverVersionComboBox.addItem(version[0]);
                }
                
                String[] serverType = setting.split("=");
                int serverInt = 1;

                if (serverType[1].equals("vanilla")) serverInt = 0;
                if (serverType[1].equals("bukkit")) serverInt = 1;
                if (serverType[1].equals("spigot")) serverInt = 2;
                if (serverType[1].equals("sponge")) serverInt = 3;
                if (serverType[1].equals("glowstone")) serverInt = 4;

                Component[] components = maintainancePanel.getComponents();
                
                switch (serverInt) {
                    case 0:
                        for (Component component : components) {
                            component.setEnabled(true);
                        }
                        
                        retrieveCheckBox.setEnabled(false);
                        notifyCheckBox.setEnabled(false);
                        updateCheckBox.setEnabled(false);
                        downloadBetaButton.setEnabled(false);
                        downloadDevButton.setEnabled(false);
                        
                        String version[] = vanillaVersions.get(0).split(": ");
                        latestStableLabel.setText("Latest Stable: " + version[0]);
                        
                        serverComboBox.setSelectedIndex(0);
                        serverTypeImage.setIcon(new ImageIcon(getClass().getResource("/org/another_developer/bukkitgui/resources/vanilla_logo.png")));
                        linkLabel.setText("<html><a href=\"https://minecraft.net/\">minecraft.net</a></html>");
                        break;
                    case 1:
                        for (Component component : components) {
                            component.setEnabled(false);
                        }
                        
                        serverComboBox.setSelectedIndex(1);
                        serverTypeImage.setIcon(new ImageIcon(getClass().getResource("/org/another_developer/bukkitgui/resources/bukkit_logo.png")));
                        linkLabel.setText("<html><a href=\"http://bukkit.org/\">bukkit.org</a></html>");
                        break;
                    case 2:
                        for (Component component : components) {
                            component.setEnabled(false);
                        }
                        
                        serverComboBox.setSelectedIndex(2);
                        serverTypeImage.setIcon(new ImageIcon(getClass().getResource("/org/another_developer/bukkitgui/resources/spigot_logo.png")));
                        linkLabel.setText("<html><a href=\"https://www.spigotmc.org/\">spigotmc.org</a></html>");
                        break;
                    case 3:
                        for (Component component : components) {
                            component.setEnabled(false);
                        }
                        
                        serverComboBox.setSelectedIndex(3);
                        serverTypeImage.setIcon(new ImageIcon(getClass().getResource("/org/another_developer/bukkitgui/resources/sponge_logo.png")));
                        linkLabel.setText("<html><a href=\"https://www.spongepowered.org/\">spongepowered.org</a></html>");
                        break;
                    case 4:
                        for (Component component : components) {
                            component.setEnabled(false);
                        }
                        
                        serverComboBox.setSelectedIndex(4);
                        serverTypeImage.setIcon(new ImageIcon(getClass().getResource("/org/another_developer/bukkitgui/resources/glowstone_logo.png")));
                        linkLabel.setText("<html><a href=\"https://www.glowstone.net/\">glowstone.net</a></html>");
                        break;
                }
            }
            
            if (setting.startsWith("remoteType=")) {
                String[] serverType = setting.split("=");
                int remoteInt = 1;

                if (serverType[1].equals("SSH")) remoteInt = 0;
                if (serverType[1].equals("RemoteToolKit")) remoteInt = 1;
                if (serverType[1].equals("JSONAPI")) remoteInt = 2;
                if (serverType[1].equals("RemoteBukkit")) remoteInt = 3;

                switch (remoteInt) {
                    case 0:
                        remoteTypeComboBox.setSelectedIndex(0);
                        break;
                    case 1:
                        remoteTypeComboBox.setSelectedIndex(1);
                        break;
                    case 2:
                        remoteTypeComboBox.setSelectedIndex(2);
                        break;
                    case 3:
                        remoteTypeComboBox.setSelectedIndex(3);
                        break;
                    case 4:
                        remoteTypeComboBox.setSelectedIndex(4);
                        break;
                }
            }

            remoteCheckBoxActionPerformed(null);
        }
    }
    
    /**
     * Updates server arguments
     */
    public void updateArgs() {
        if (!remoteCheckBox.isSelected()) {
            Server.updateArgs(remoteCheckBox.isSelected(), (String) javaVersionComboBox.getSelectedItem(), customArgumentsTextField.getText(), jarFileTextField.getText(), customSwitchesTextField.getText());
        } else {
            Server.updateArgs(remoteCheckBox.isSelected(), (String) javaVersionComboBox.getSelectedItem(), customArgumentsTextField.getText(), jarRemoteFileTextField.getText(), customSwitchesTextField.getText());
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        remoteTypeLabel = new javax.swing.JLabel();
        remoteTypeComboBox = new javax.swing.JComboBox();
        maintainancePanel = new javax.swing.JPanel();
        serverTypeImage = new javax.swing.JLabel();
        latestStableLabel = new javax.swing.JLabel();
        latestBetaLabel = new javax.swing.JLabel();
        latestDevLabel = new javax.swing.JLabel();
        currentVersionButton = new javax.swing.JButton();
        downloadRecomendedButton = new javax.swing.JButton();
        downloadBetaButton = new javax.swing.JButton();
        downloadDevButton = new javax.swing.JButton();
        downloadLabel = new javax.swing.JLabel();
        downloadButton = new javax.swing.JButton();
        siteLabel = new javax.swing.JLabel();
        linkLabel = new javax.swing.JLabel();
        retrieveCheckBox = new javax.swing.JCheckBox();
        notifyCheckBox = new javax.swing.JCheckBox();
        updateCheckBox = new javax.swing.JCheckBox();
        serverVersionComboBox = new javax.swing.JComboBox();
        serverPanel = new javax.swing.JPanel();
        javaVersionComboBox = new javax.swing.JComboBox();
        javaVersionLabel = new javax.swing.JLabel();
        minRAMSlider = new javax.swing.JSlider();
        minRAMLabel = new javax.swing.JLabel();
        maxRAMLabel = new javax.swing.JLabel();
        maxRAMSlider = new javax.swing.JSlider();
        jarFileTextField = new javax.swing.JTextField();
        customArgumentsTextField = new javax.swing.JTextField();
        customSwitchesTextField = new javax.swing.JTextField();
        jarFileLabel = new javax.swing.JLabel();
        customArgumentsLabel = new javax.swing.JLabel();
        customSwitchesLabel = new javax.swing.JLabel();
        jarFileButton = new javax.swing.JButton();
        remoteCheckBox = new javax.swing.JCheckBox();
        startButton = new javax.swing.JButton();
        portforwardButton = new javax.swing.JButton();
        remotePanel = new javax.swing.JPanel();
        hostLabel = new javax.swing.JLabel();
        userLabel = new javax.swing.JLabel();
        saltLabel = new javax.swing.JLabel();
        portLabel = new javax.swing.JLabel();
        passwordLabel = new javax.swing.JLabel();
        portSpinner = new javax.swing.JSpinner();
        passwordField = new javax.swing.JPasswordField();
        hostTextField = new javax.swing.JTextField();
        userTextField = new javax.swing.JTextField();
        saltTextField = new javax.swing.JTextField();
        jarRemoteFileLabel = new javax.swing.JLabel();
        jarRemoteFileButton = new javax.swing.JButton();
        jarRemoteFileTextField = new javax.swing.JTextField();
        connectButton = new javax.swing.JButton();
        serverTypeLabel = new javax.swing.JLabel();
        serverComboBox = new javax.swing.JComboBox();

        remoteTypeLabel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        remoteTypeLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        remoteTypeLabel.setText("Remote Type:");

        remoteTypeComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "SSH", "RemoteToolKit", "JSONAPI", "RemoteBukkit" }));
        remoteTypeComboBox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                remoteTypeComboBoxItemStateChanged(evt);
            }
        });

        maintainancePanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Maintainance", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 0, 11))); // NOI18N

        serverTypeImage.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        serverTypeImage.setMaximumSize(new java.awt.Dimension(128, 128));
        serverTypeImage.setMinimumSize(new java.awt.Dimension(128, 128));
        serverTypeImage.setPreferredSize(new java.awt.Dimension(128, 128));

        latestStableLabel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        latestStableLabel.setText("Latest Stable:");

        latestBetaLabel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        latestBetaLabel.setText("Latest Beta:");

        latestDevLabel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        latestDevLabel.setText("Latest Dev:");

        currentVersionButton.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        currentVersionButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/another_developer/bukkitgui/resources/information.png"))); // NOI18N
        currentVersionButton.setText("Get current version #");
        currentVersionButton.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        currentVersionButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                currentVersionButtonActionPerformed(evt);
            }
        });

        downloadRecomendedButton.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        downloadRecomendedButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/another_developer/bukkitgui/resources/drive-download.png"))); // NOI18N
        downloadRecomendedButton.setText("Download Latest Recomended Build");
        downloadRecomendedButton.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        downloadBetaButton.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        downloadBetaButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/another_developer/bukkitgui/resources/drive-download.png"))); // NOI18N
        downloadBetaButton.setText("Download Latest Beta Build");
        downloadBetaButton.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        downloadDevButton.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        downloadDevButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/another_developer/bukkitgui/resources/drive-download.png"))); // NOI18N
        downloadDevButton.setText("Download Latest Development Build");
        downloadDevButton.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        downloadLabel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        downloadLabel.setText("Download #:");

        downloadButton.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        downloadButton.setText("Download");
        downloadButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                downloadButtonActionPerformed(evt);
            }
        });

        siteLabel.setText("Site:");

        linkLabel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        linkLabel.setEnabled(false);

        retrieveCheckBox.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        retrieveCheckBox.setText("Retrieve the current version on server start");

        notifyCheckBox.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        notifyCheckBox.setText("Notify me when a server update available");

        updateCheckBox.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        updateCheckBox.setText("Auto-update when an update is available");

        javax.swing.GroupLayout maintainancePanelLayout = new javax.swing.GroupLayout(maintainancePanel);
        maintainancePanel.setLayout(maintainancePanelLayout);
        maintainancePanelLayout.setHorizontalGroup(
            maintainancePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(maintainancePanelLayout.createSequentialGroup()
                .addComponent(serverTypeImage, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(maintainancePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(latestStableLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(latestBetaLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(latestDevLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(currentVersionButton, javax.swing.GroupLayout.DEFAULT_SIZE, 222, Short.MAX_VALUE)))
            .addComponent(downloadRecomendedButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(downloadBetaButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(downloadDevButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(maintainancePanelLayout.createSequentialGroup()
                .addComponent(downloadLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(serverVersionComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(downloadButton, javax.swing.GroupLayout.PREFERRED_SIZE, 26, Short.MAX_VALUE))
            .addGroup(maintainancePanelLayout.createSequentialGroup()
                .addComponent(siteLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(linkLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(retrieveCheckBox, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(notifyCheckBox, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(updateCheckBox, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        maintainancePanelLayout.setVerticalGroup(
            maintainancePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(maintainancePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(maintainancePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(maintainancePanelLayout.createSequentialGroup()
                        .addComponent(latestStableLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(latestBetaLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(latestDevLabel)
                        .addGap(34, 34, 34)
                        .addComponent(currentVersionButton))
                    .addComponent(serverTypeImage, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(downloadRecomendedButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(downloadBetaButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(downloadDevButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(maintainancePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(serverVersionComboBox, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(maintainancePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(downloadLabel)
                        .addComponent(downloadButton)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(maintainancePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(siteLabel)
                    .addComponent(linkLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(retrieveCheckBox)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(notifyCheckBox)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(updateCheckBox))
        );

        serverPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Java Server", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 0, 11))); // NOI18N

        javaVersionComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Default" }));

        javaVersionLabel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        javaVersionLabel.setText("Java Version:");

        minRAMSlider.setMaximum((int) ((((OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean()).getTotalPhysicalMemorySize() / 1024) / 1024));
        minRAMSlider.setMinorTickSpacing(256);
        minRAMSlider.setPaintTicks(true);
        minRAMSlider.setValue(0);
        minRAMSlider.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                minRAMSliderStateChanged(evt);
            }
        });

        minRAMLabel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        minRAMLabel.setText("Min. RAM:");

        maxRAMLabel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        maxRAMLabel.setText("Max. RAM:");

        maxRAMSlider.setMaximum((int) ((((OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean()).getTotalPhysicalMemorySize() / 1024) / 1024));
        maxRAMSlider.setMinorTickSpacing(256);
        maxRAMSlider.setPaintTicks(true);
        maxRAMSlider.setValue(0);
        maxRAMSlider.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                maxRAMSliderStateChanged(evt);
            }
        });

        customArgumentsTextField.setText("-Xmx0M -Xms0M ");
        customArgumentsTextField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                customArgumentsTextFieldFocusLost(evt);
            }
        });
        customArgumentsTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                customArgumentsTextFieldActionPerformed(evt);
            }
        });

        jarFileLabel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jarFileLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jarFileLabel.setText("Jar File:");

        customArgumentsLabel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        customArgumentsLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        customArgumentsLabel.setText("Custom Arguments:");

        customSwitchesLabel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        customSwitchesLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        customSwitchesLabel.setText("Custom Switches:");

        jarFileButton.setText("...");
        jarFileButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jarFileButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout serverPanelLayout = new javax.swing.GroupLayout(serverPanel);
        serverPanel.setLayout(serverPanelLayout);
        serverPanelLayout.setHorizontalGroup(
            serverPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(serverPanelLayout.createSequentialGroup()
                .addGroup(serverPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(maxRAMLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 123, Short.MAX_VALUE)
                    .addComponent(minRAMLabel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(javaVersionLabel, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jarFileLabel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(serverPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(serverPanelLayout.createSequentialGroup()
                        .addGroup(serverPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(serverPanelLayout.createSequentialGroup()
                                .addComponent(jarFileTextField)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jarFileButton))
                            .addComponent(javaVersionComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addContainerGap())
                    .addComponent(minRAMSlider, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(maxRAMSlider, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, serverPanelLayout.createSequentialGroup()
                .addGroup(serverPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(customArgumentsLabel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(customSwitchesLabel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(serverPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(customArgumentsTextField)
                    .addComponent(customSwitchesTextField))
                .addContainerGap())
        );
        serverPanelLayout.setVerticalGroup(
            serverPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(serverPanelLayout.createSequentialGroup()
                .addGroup(serverPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(javaVersionLabel)
                    .addComponent(javaVersionComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(serverPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(minRAMSlider, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(minRAMLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(serverPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(maxRAMLabel)
                    .addComponent(maxRAMSlider, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(serverPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jarFileTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jarFileLabel)
                    .addComponent(jarFileButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(serverPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(customArgumentsTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(customArgumentsLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(serverPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(customSwitchesTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(customSwitchesLabel))
                .addContainerGap())
        );

        remoteCheckBox.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        remoteCheckBox.setText("Remote Server");
        remoteCheckBox.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        remoteCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                remoteCheckBoxActionPerformed(evt);
            }
        });

        startButton.setText("Launch Server");
        startButton.setMaximumSize(new java.awt.Dimension(470, 25));
        startButton.setMinimumSize(new java.awt.Dimension(47, 25));
        startButton.setPreferredSize(new java.awt.Dimension(470, 25));
        startButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startButtonActionPerformed(evt);
            }
        });

        portforwardButton.setText("Setup Port Forwarding");
        portforwardButton.setMaximumSize(new java.awt.Dimension(470, 25));
        portforwardButton.setMinimumSize(new java.awt.Dimension(47, 25));
        portforwardButton.setPreferredSize(new java.awt.Dimension(470, 25));
        portforwardButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                portforwardButtonActionPerformed(evt);
            }
        });

        remotePanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Remote Server", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 0, 11))); // NOI18N

        hostLabel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        hostLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        hostLabel.setText("Host:");

        userLabel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        userLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        userLabel.setText("Username:");

        saltLabel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        saltLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        saltLabel.setText("Salt:");

        portLabel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        portLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        portLabel.setText("Port:");

        passwordLabel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        passwordLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        passwordLabel.setText("Password:");

        portSpinner.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        portSpinner.setModel(new javax.swing.SpinnerNumberModel(Integer.valueOf(22), null, null, Integer.valueOf(0)));
        portSpinner.setEditor(new javax.swing.JSpinner.NumberEditor(portSpinner, ""));

        jarRemoteFileLabel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jarRemoteFileLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jarRemoteFileLabel.setText("Jar File:");

        jarRemoteFileButton.setText("...");
        jarRemoteFileButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jarRemoteFileButtonActionPerformed(evt);
            }
        });

        connectButton.setText("Connect");
        connectButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                connectButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout remotePanelLayout = new javax.swing.GroupLayout(remotePanel);
        remotePanel.setLayout(remotePanelLayout);
        remotePanelLayout.setHorizontalGroup(
            remotePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(remotePanelLayout.createSequentialGroup()
                .addGroup(remotePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(remotePanelLayout.createSequentialGroup()
                        .addComponent(jarRemoteFileLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jarRemoteFileTextField)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jarRemoteFileButton))
                    .addGroup(remotePanelLayout.createSequentialGroup()
                        .addGroup(remotePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(saltLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(userLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(hostLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(remotePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(hostTextField, javax.swing.GroupLayout.DEFAULT_SIZE, 190, Short.MAX_VALUE)
                            .addComponent(userTextField, javax.swing.GroupLayout.DEFAULT_SIZE, 190, Short.MAX_VALUE)
                            .addComponent(saltTextField))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(remotePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(passwordLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(portLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(remotePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(portSpinner, javax.swing.GroupLayout.DEFAULT_SIZE, 203, Short.MAX_VALUE)
                            .addComponent(passwordField)
                            .addComponent(connectButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        remotePanelLayout.setVerticalGroup(
            remotePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(remotePanelLayout.createSequentialGroup()
                .addGroup(remotePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(hostLabel)
                    .addComponent(portLabel)
                    .addComponent(portSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(hostTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(remotePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(userLabel)
                    .addComponent(passwordLabel)
                    .addComponent(passwordField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(userTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(remotePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(saltLabel)
                    .addComponent(saltTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(connectButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(remotePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jarRemoteFileButton)
                    .addComponent(jarRemoteFileTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jarRemoteFileLabel)))
        );

        serverTypeLabel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        serverTypeLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        serverTypeLabel.setText("Server Type:");

        serverComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Vanilla", "Bukkit", "Spigot", "Sponge", "Glowstone" }));
        serverComboBox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                serverComboBoxItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(serverPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(startButton, javax.swing.GroupLayout.DEFAULT_SIZE, 571, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(remoteCheckBox)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(remoteTypeLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(remoteTypeComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(serverTypeLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(serverComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addComponent(remotePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(maintainancePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(portforwardButton, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(3, 3, 3)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(serverTypeLabel)
                                .addComponent(serverComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(remoteTypeLabel)
                                .addComponent(remoteTypeComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(remoteCheckBox)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(serverPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(remotePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(maintainancePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(portforwardButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(startButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    /* <editor-fold>
     * Gets all available Java versions from the local system or remote system.
     *///</editor-fold>
    private void getJava() {
//      FIXME: Once again, a string switch statement would be nice here ... cursing 1.6!
//      TODO: Get sub keys (WinRegistry.readStringSubKeys(HKEY, key))
        if (!remoteCheckBox.isSelected()) {
            javaVersionComboBox.removeAllItems();

            if (BukkitGUI.OS.contains("Windows")) {
                String[] registryPaths = {
                        //32-bit
                        "SOFTWARE\\JavaSoft\\Java Runtime Environment",
                        "SOFTWARE\\JavaSoft\\Java Development Kit",

                        //64-bit
                        "SOFTWARE\\Wow6432Node\\JavaSoft\\Java Runtime Environment",
                        "SOFTWARE\\Wow6432Node\\JavaSoft\\Java Development Kit",
                };

                try {
                    String currentVersion = WinRegistry.readString(WinRegistry.HKEY_LOCAL_MACHINE, registryPaths[0], "CurrentVersion");

                    if (!currentVersion.equals("")) {
                        String javaHome = WinRegistry.readString(WinRegistry.HKEY_LOCAL_MACHINE, registryPaths[0] + "\\" + currentVersion, "JavaHome");

                        if (!javaHome.equals("")) {
                            javaVersionComboBox.addItem(javaHome);
                        }
                    }

                    currentVersion = WinRegistry.readString(WinRegistry.HKEY_LOCAL_MACHINE, registryPaths[1], "CurrentVersion");

                    if (!currentVersion.equals("")) {
                        String javaHome = WinRegistry.readString(WinRegistry.HKEY_LOCAL_MACHINE, registryPaths[1] + "\\" + currentVersion, "JavaHome");

                        if (!javaHome.equals("")) {
                            javaVersionComboBox.addItem(javaHome);
                        }
                    }

                    currentVersion = WinRegistry.readString(WinRegistry.HKEY_LOCAL_MACHINE, registryPaths[2], "CurrentVersion");

                    if (!currentVersion.equals("")) {
                        String javaHome = WinRegistry.readString(WinRegistry.HKEY_LOCAL_MACHINE, registryPaths[2] + "\\" + currentVersion, "JavaHome");

                        if (!javaHome.equals("")) {
                            javaVersionComboBox.addItem(javaHome);
                        }
                    }

                    currentVersion = WinRegistry.readString(WinRegistry.HKEY_LOCAL_MACHINE, registryPaths[3], "CurrentVersion");

                    if (!currentVersion.equals("")) {
                        String javaHome = WinRegistry.readString(WinRegistry.HKEY_LOCAL_MACHINE, registryPaths[3] + "\\" + currentVersion, "JavaHome");

                        if (!javaHome.equals("")) {
                            javaVersionComboBox.addItem(javaHome);
                        }
                    }
                } catch (Exception ex) {
                    log.log(Level.SEVERE, getStackTrace(ex.getCause()));
                }
            } else if (BukkitGUI.OS.contains("Linux")) {
                BufferedReader in = null;
                String inputLine, data = "";
                ArrayList<String> javaVersions;

                try {
                    if (new File("/var/lib/alternatives/java").exists()) {
                        in = new BufferedReader(new FileReader(new File("/var/lib/alternatives/java")));
                    } else if (new File("/var/lib/dpkg/alternatives/java").exists()) {
                        in = new BufferedReader(new FileReader(new File("/var/lib/dpkg/alternatives/java")));
                    }

                    javaVersionComboBox.removeAllItems();

                    while ((inputLine = in.readLine()) != null) {
                        data = data + "\n" + inputLine;

                        if (inputLine.startsWith("/") && !inputLine.endsWith(".gz")) {
                            javaVersionComboBox.addItem(inputLine);
                        }
                    }
                    in.close();
                } catch (Exception ex) {
                    log.log(Level.SEVERE, getStackTrace(ex));
                }
            } else if (BukkitGUI.OS.contains("Mac OS X")) {
                if (new File("/System/Library/Java/JavaVirtualMachines").exists()) {
                    log.log(Level.INFO, "System JRE folder exists");

                    File java = new File("/System/Library/Java/JavaVirtualMachines");
                    ArrayList javaVersions = new ArrayList<String>(Arrays.asList(java.list()));

                    javaVersionComboBox.setModel(new DefaultComboBoxModel());
                    for (Object item : javaVersions) {
                        javaVersionComboBox.addItem("/System/Library/Java/JavaVirtualMachines/" + item);
                    }
                } else if (new File("/Library/Java/JavaVirtualMachines").exists()) {
                    log.log(Level.INFO, "Additional JRE folder exists");

                    File java = new File("/Library/Java/JavaVirtualMachines");
                    ArrayList javaVersions = new ArrayList<String>(Arrays.asList(java.list()));

                    javaVersionComboBox.setModel(new DefaultComboBoxModel());
                    for (Object item : javaVersions) {
                        javaVersionComboBox.addItem("/Library/Java/JavaVirtualMachines/" + item);
                    }
                } else if (new File(System.getProperty("user.home") + "/Library/Java/JavaVirtualMachines").exists()) {
                    log.log(Level.INFO, "User JRE folder exists");

                    File java = new File(System.getProperty("user.home") + "/Library/Java/JavaVirtualMachines");
                    ArrayList javaVersions = new ArrayList<String>(Arrays.asList(java.list()));

                    javaVersionComboBox.setModel(new DefaultComboBoxModel());
                    for (Object item : javaVersions) {
                        javaVersionComboBox.addItem(System.getProperty("user.home") + "/Library/Java/JavaVirtualMachines/" + item);
                    }
                }
            }
        } else {
            javaVersionComboBox.removeAllItems();
            
            String[] remoteJava = Server.getJava();

            for (String item : remoteJava) {
                javaVersionComboBox.addItem(item);
            }
        }
    }

    private void jarFileButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jarFileButtonActionPerformed
        try {
            JFileChooser fileChooser = new JFileChooser(new File(TabSuperStart.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath()).toString());
            FileNameExtensionFilter filter = new FileNameExtensionFilter("Java Executable", "jar");

            fileChooser.setFileFilter(filter);
            int returnVal = fileChooser.showOpenDialog(null);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                jarFileTextField.setText(fileChooser.getSelectedFile().getAbsolutePath());
            }
        } catch (Exception ex) {
            log.log(Level.SEVERE, getStackTrace(ex));
        }
    }//GEN-LAST:event_jarFileButtonActionPerformed

    private boolean shownOnce = false;
    
    private void remoteTypeComboBoxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_remoteTypeComboBoxItemStateChanged
        Component[] components = remotePanel.getComponents();
        
        switch(remoteTypeComboBox.getSelectedIndex()) {
            case 0:
                for (Component component : components) {
                    component.setEnabled(remoteCheckBox.isSelected());
                }
                
                saltLabel.setEnabled(false);
                saltTextField.setEnabled(false);
                
                break;
            case 1:
                for (Component component : components) {
                    component.setEnabled(false);
                }
                
                if (!shownOnce) {
                    javax.swing.JOptionPane.showMessageDialog(null, "The selected remote connection type\nisn't supported yet. It will be\nadded in an up comming version.", "Not Supported", JOptionPane.ERROR_MESSAGE);
                    
                    shownOnce = true;
                }
                
                break;
            case 2:
                for (Component component : components) {
                    component.setEnabled(false);
                }
                
                if (!shownOnce) {
                    javax.swing.JOptionPane.showMessageDialog(null, "The selected remote connection type\nisn't supported yet. It will be\nadded in an up comming version.", "Not Supported", JOptionPane.ERROR_MESSAGE);
                    
                    shownOnce = true;
                }
                
                break;
            case 3:
                for (Component component : components) {
                    component.setEnabled(false);
                }
                
                if (!shownOnce) {
                    javax.swing.JOptionPane.showMessageDialog(null, "The selected remote connection type\nisn't supported yet. It will be\nadded in an up comming version.", "Not Supported", JOptionPane.ERROR_MESSAGE);
                    
                    shownOnce = true;
                }
                
                break;
        }
        
        for (int i = 0; i <= settings.size() - 1; i++) {
            if (settings.get(i).startsWith("remoteType=")) {
                settings.set(i, "remoteType=" + remoteTypeComboBox.getSelectedItem());
            }
        }
    }//GEN-LAST:event_remoteTypeComboBoxItemStateChanged

    private void startButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_startButtonActionPerformed
        if (!remoteCheckBox.isSelected()) {
            Server.startServer(remoteCheckBox.isSelected(), 0, serverComboBox.getSelectedIndex(), (String) javaVersionComboBox.getSelectedItem(), customArgumentsTextField.getText(), jarFileTextField.getText(), customSwitchesTextField.getText());
        } else {
            Server.startServer(remoteCheckBox.isSelected(), 0, serverComboBox.getSelectedIndex(), (String) javaVersionComboBox.getSelectedItem(), customArgumentsTextField.getText(), jarRemoteFileTextField.getText(), customSwitchesTextField.getText());
        }
        BaseFrame.baseTabs.setSelectedIndex(0);
    }//GEN-LAST:event_startButtonActionPerformed

    private void portforwardButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_portforwardButtonActionPerformed
        Server.setupPortForwarding();
    }//GEN-LAST:event_portforwardButtonActionPerformed

    private void jarRemoteFileButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jarRemoteFileButtonActionPerformed
        jarRemoteFileTextField.setText(Server.getServer());
    }//GEN-LAST:event_jarRemoteFileButtonActionPerformed

    private void remoteCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_remoteCheckBoxActionPerformed
        remoteTypeComboBoxItemStateChanged(null);
        
        jarFileTextField.setEnabled(!remoteCheckBox.isSelected());
        jarFileButton.setEnabled(!remoteCheckBox.isSelected());
        remoteTypeLabel.setEnabled(remoteCheckBox.isSelected());
        remoteTypeComboBox.setEnabled(remoteCheckBox.isSelected());

        if (remoteCheckBox.isSelected()) javaVersionComboBox.removeAllItems();
        if (!remoteCheckBox.isSelected()) {
            Component[] components = remotePanel.getComponents();
            for (Component component : components) {
                component.setEnabled(false);
            }
            
            getJava();
        }

        for (int i = 0; i <= settings.size() - 1; i++) {
            if (settings.get(i).startsWith("remote=")) {
                settings.set(i, "remote=" + remoteCheckBox.isSelected());
                //FIXME: At init the true value of remoteCheckBox gets annihilated
            }
        }
    }//GEN-LAST:event_remoteCheckBoxActionPerformed

    private void maxRAMSliderStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_maxRAMSliderStateChanged
        maxRAMLabel.setText("Max. RAM: " + maxRAMSlider.getValue() + " Mb");

        if (maxRAMSlider.getValue() <= minRAMSlider.getValue()) minRAMSlider.setValue(maxRAMSlider.getValue());

        customArgumentsTextField.setText(customArgumentsTextField.getText().replace(argsMax, "-Xmx" + maxRAMSlider.getValue() + "M "));
        argsMax = "-Xmx" + maxRAMSlider.getValue() + "M ";
    }//GEN-LAST:event_maxRAMSliderStateChanged

    private void minRAMSliderStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_minRAMSliderStateChanged
        minRAMLabel.setText("Min. RAM: " + minRAMSlider.getValue() + " Mb");

        if (minRAMSlider.getValue() >= maxRAMSlider.getValue()) maxRAMSlider.setValue(minRAMSlider.getValue());

        customArgumentsTextField.setText(customArgumentsTextField.getText().replace(argsMin, "-Xms" + minRAMSlider.getValue() + "M "));
        argsMin = "-Xms" + minRAMSlider.getValue() + "M ";
    }//GEN-LAST:event_minRAMSliderStateChanged

    private void customArgumentsTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_customArgumentsTextFieldActionPerformed
        String[] args = customArgumentsTextField.getText().split(" -");
        for (String arg : args) {
            if (arg.startsWith("Xmx")) {
                maxRAMSlider.setValue(new Integer(arg.replace("Xmx", "")
                        .replace("M", "")
                        .replace("m", "")
                        .replace("G", "")
                        .replace("g", "")
                        .replace(" ", "")));
            } else if (arg.startsWith("Xms")) {
                minRAMSlider.setValue(new Integer(arg.replace("Xms", "")
                        .replace("M", "")
                        .replace("m", "")
                        .replace("G", "")
                        .replace("g", "")
                        .replace(" ", "")));
            }
        }
    }//GEN-LAST:event_customArgumentsTextFieldActionPerformed

    private void customArgumentsTextFieldFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_customArgumentsTextFieldFocusLost
        customArgumentsTextFieldActionPerformed(null);
    }//GEN-LAST:event_customArgumentsTextFieldFocusLost

    private void connectButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_connectButtonActionPerformed
        if (!Server.remote && !hostTextField.getText().isEmpty() && !userTextField.getText().isEmpty())
            Server.connectToServer(userTextField.getText(), passwordField.getPassword(), saltTextField.getText(), hostTextField.getText(), (Integer) portSpinner.getValue());
        if (Server.remote) getJava();
    }//GEN-LAST:event_connectButtonActionPerformed

    private void serverComboBoxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_serverComboBoxItemStateChanged
        Component[] components = maintainancePanel.getComponents();
        
        switch (serverComboBox.getSelectedIndex()) {
            case 0:
                for (Component component : components) {
                    component.setEnabled(true);
                }

                retrieveCheckBox.setEnabled(false);
                notifyCheckBox.setEnabled(false);
                updateCheckBox.setEnabled(false);
                downloadBetaButton.setEnabled(false);
                downloadDevButton.setEnabled(false);
                
                String version[] = vanillaVersions.get(0).split(": ");
                latestStableLabel.setText("Latest Stable: " + version[0]);

                serverTypeImage.setIcon(new ImageIcon(getClass().getResource("/org/another_developer/bukkitgui/resources/vanilla_logo.png")));
                linkLabel.setText("<html><a href=\"https://minecraft.net/\">minecraft.net</a></html>");
                break;
            case 1:
                for (Component component : components) {
                    component.setEnabled(false);
                }

                serverTypeImage.setIcon(new ImageIcon(getClass().getResource("/org/another_developer/bukkitgui/resources/bukkit_logo.png")));
                linkLabel.setText("<html><a href=\"http://bukkit.org/\">bukkit.org</a></html>");
                break;
            case 2:
                for (Component component : components) {
                    component.setEnabled(false);
                }

                serverTypeImage.setIcon(new ImageIcon(getClass().getResource("/org/another_developer/bukkitgui/resources/spigot_logo.png")));
                linkLabel.setText("<html><a href=\"https://www.spigotmc.org/\">spigotmc.org</a></html>");
                break;
            case 3:
                for (Component component : components) {
                    component.setEnabled(false);
                }

                serverTypeImage.setIcon(new ImageIcon(getClass().getResource("/org/another_developer/bukkitgui/resources/sponge_logo.png")));
                linkLabel.setText("<html><a href=\"https://www.spongepowered.org/\">spongepowered.org</a></html>");
                break;
            case 4:
                for (Component component : components) {
                    component.setEnabled(false);
                }

                serverTypeImage.setIcon(new ImageIcon(getClass().getResource("/org/another_developer/bukkitgui/resources/glowstone_logo.png")));
                linkLabel.setText("<html><a href=\"https://www.glowstone.net/\">glowstone.net</a></html>");
                break;
        }

        for (int i = 0; i <= settings.size() - 1; i++) {
            if (settings.get(i).startsWith("serverType=")) {
                switch (serverComboBox.getSelectedIndex()) {
                    case 0:
                        settings.set(i, "serverType=vanilla");
                        break;
                    case 1:
                        settings.set(i, "serverType=bukkit");
                        break;
                    case 2:
                        settings.set(i, "serverType=spigot");
                        break;
                    case 3:
                        settings.set(i, "serverType=sponge");
                        break;
                    case 4:
                        settings.set(i, "serverType=glowstone");
                        break;
                }
            }
        }
    }//GEN-LAST:event_serverComboBoxItemStateChanged

    private void currentVersionButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_currentVersionButtonActionPerformed
        try {
            String serverDir[] = jarFileTextField.getText().split(File.separator);
            serverDir[serverDir.length - 1] = "";

            File serverLog = new File(Arrays.toString(serverDir).replace("[", "").replace("]", "").replace(", ", File.separator) + File.separator + "logs" + File.separator + "latest.log");

            if (!serverLog.exists() && !jarFileTextField.getText().isEmpty()) {
                JOptionPane.showMessageDialog(this, serverLog + " doesn't exist.\n The server needs to have ran at least once.", "Can't detect version", JOptionPane.ERROR_MESSAGE);
            }
            
            if (jarFileTextField.getText().isEmpty()) {
                JOptionPane.showMessageDialog(this, "You need to have selected a server jar file.", "No jar file set", JOptionPane.INFORMATION_MESSAGE);
            }
        
            if (serverLog.exists() && !jarFileTextField.getText().isEmpty()) {
                BufferedReader in = new BufferedReader(new FileReader(serverLog));
                String inputLine;
                
                if ((inputLine = in.readLine()) != null) {
                    String[] version = inputLine.split(" ");
                    
                    serverVersionComboBox.setSelectedIndex(vanillaVersions.indexOf(version[version.length - 1] + ": https://s3.amazonaws.com/Minecraft.Download/versions/" + version[version.length - 1] + "/minecraft_server." + version[version.length - 1] + ".jar"));

                    in.close();
                }
            }
        } catch (Exception ex) {
            log.log(Level.SEVERE, getStackTrace(ex));
        }
    }//GEN-LAST:event_currentVersionButtonActionPerformed

    private void downloadButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_downloadButtonActionPerformed
        Object[] options = {"Yes", "No"};
        int i = JOptionPane.showOptionDialog(null, "Are you sure you want to proceed?\n"
                                                 + "If there is conflict between file\n"
                                                 + "<html><p>names, the old server <b>will</b> be</p></html>\n"
                                                 + "over-written.", "Warning", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
        
        if (i == 0) {
            updateArgs();
            Server.udpateServer(vanillaVersions.get(serverVersionComboBox.getSelectedIndex()));
        }
    }//GEN-LAST:event_downloadButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton connectButton;
    private javax.swing.JButton currentVersionButton;
    private javax.swing.JLabel customArgumentsLabel;
    private javax.swing.JTextField customArgumentsTextField;
    private javax.swing.JLabel customSwitchesLabel;
    private javax.swing.JTextField customSwitchesTextField;
    private javax.swing.JButton downloadBetaButton;
    private javax.swing.JButton downloadButton;
    private javax.swing.JButton downloadDevButton;
    private javax.swing.JLabel downloadLabel;
    private javax.swing.JButton downloadRecomendedButton;
    private javax.swing.JLabel hostLabel;
    private javax.swing.JTextField hostTextField;
    private javax.swing.JButton jarFileButton;
    private javax.swing.JLabel jarFileLabel;
    private javax.swing.JTextField jarFileTextField;
    private javax.swing.JButton jarRemoteFileButton;
    private javax.swing.JLabel jarRemoteFileLabel;
    private javax.swing.JTextField jarRemoteFileTextField;
    private javax.swing.JComboBox javaVersionComboBox;
    private javax.swing.JLabel javaVersionLabel;
    private javax.swing.JLabel latestBetaLabel;
    private javax.swing.JLabel latestDevLabel;
    private javax.swing.JLabel latestStableLabel;
    private javax.swing.JLabel linkLabel;
    private javax.swing.JPanel maintainancePanel;
    private javax.swing.JLabel maxRAMLabel;
    private javax.swing.JSlider maxRAMSlider;
    private javax.swing.JLabel minRAMLabel;
    private javax.swing.JSlider minRAMSlider;
    private javax.swing.JCheckBox notifyCheckBox;
    private javax.swing.JPasswordField passwordField;
    private javax.swing.JLabel passwordLabel;
    private javax.swing.JLabel portLabel;
    private javax.swing.JSpinner portSpinner;
    private javax.swing.JButton portforwardButton;
    private javax.swing.JCheckBox remoteCheckBox;
    private javax.swing.JPanel remotePanel;
    private javax.swing.JComboBox remoteTypeComboBox;
    private javax.swing.JLabel remoteTypeLabel;
    private javax.swing.JCheckBox retrieveCheckBox;
    private javax.swing.JLabel saltLabel;
    private javax.swing.JTextField saltTextField;
    private javax.swing.JComboBox serverComboBox;
    private javax.swing.JPanel serverPanel;
    private javax.swing.JLabel serverTypeImage;
    private javax.swing.JLabel serverTypeLabel;
    private javax.swing.JComboBox serverVersionComboBox;
    private javax.swing.JLabel siteLabel;
    private javax.swing.JButton startButton;
    private javax.swing.JCheckBox updateCheckBox;
    private javax.swing.JLabel userLabel;
    private javax.swing.JTextField userTextField;
    // End of variables declaration//GEN-END:variables
}