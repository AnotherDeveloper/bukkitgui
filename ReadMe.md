![Image not found](https://dl.dropboxusercontent.com/u/30154867/BukkitGUI/bukkitgui_banner.png)  
   
Welcome to this branch of BukkitGUI.

The intent of this project is to create a version of BukkitGUI that works cross-platform and to use the same programming language as Minecraft.

However there are few drawbacks to doing this in Java, like the JVM heap size tends to be large and not every user has the same Java version.
This project is compiled for Java version 6, as the minimum requirements Mojang sets are for Java version 6 release 45. This is done to prevent users from getting a major minor exception due to the fact they're outdated on their Java version.
Yet there are plans to also branch this project to JavaFX and possibly Android as well.

As of now, the project is heavily based off of BukkitGUI and somewhat BukkitGUI2 as well. Currently it's mostly about implementing as much features as possible that could be useful, before we or I start to optimize the GUI for greater ease of use and navigation.

###### The original tool was created by [Bertware](http://get.bertware.net/) and has been put under the [Mozilla Public License version 2 (MPL2)](https://www.mozilla.org/MPL/2.0/).